////////////////////////////////////////////////////////////////////////////
// Copyright IRISA 2017							  //
// 									  //
// triskele.obelix (at) irisa.fr					  //
// 									  //
// This  software is  a computer  program  whose purpose  is to  classify //
// pixels by using TRISKELE and random forest.				  //
// 									  //
// This software is governed by the CeCILL-B license under French law and //
// abiding by  the rules of distribution  of free software. You  can use, //
// modify  and/or  redistribute  the  software under  the  terms  of  the //
// CeCILL-B license as circulated by CEA, CNRS and INRIA at the following //
// URL "http://www.cecill.info".					  //
// 									  //
// As a counterpart to the access to  the source code and rights to copy, //
// modify and  redistribute granted  by the  license, users  are provided //
// only with a limited warranty and  the software's author, the holder of //
// the economic  rights, and the  successive licensors have  only limited //
// liability.								  //
// 									  //
// In this respect, the user's attention is drawn to the risks associated //
// with loading,  using, modifying  and/or developing or  reproducing the //
// software by the user in light of its specific status of free software, //
// that may  mean that  it is  complicated to  manipulate, and  that also //
// therefore means  that it  is reserved  for developers  and experienced //
// professionals having in-depth computer  knowledge. Users are therefore //
// encouraged  to load  and test  the software's  suitability as  regards //
// their  requirements  in  conditions  enabling the  security  of  their //
// systems and/or  data to  be ensured  and, more  generally, to  use and //
// operate it in the same conditions as regards security.		  //
// 									  //
// The fact that  you are presently reading this means  that you have had //
// knowledge of the CeCILL-B license and that you accept its terms.	  //
////////////////////////////////////////////////////////////////////////////

#include <iostream>
#include <boost/filesystem.hpp>
#include <exception>

#include <shark/Data/Dataset.h>
#include <shark/Models/Trees/RFClassifier.h>

#include "obelixDebug.hpp"
#include "IImage.hpp"

#include "OptBroceliande.hpp"
#include "Broceliande.hpp"

static const int GeoDimT (2);

namespace obelix {
  namespace broceliande {

    int createBroceliande (OptBroceliande &optBroceliande);

    template<typename PixelT>
    int createBroceliandeB (OptBroceliande &optBroceliande);

    template<typename PixelT>
    bool chooseGroundTruth (Broceliande<PixelT> &broceliande, IImage<GeoDimT> &groundTruthImage, const double &bgValue);

  } // broceliande
} // obelix

using namespace std;
using namespace boost::filesystem;
using namespace shark;

using namespace obelix;
using namespace obelix::broceliande;

// ================================================================================
int
main (int argc, char** argv, char** envp) {
  //debug = true; // XXX
  OptBroceliande optBroceliande (argc, argv);

  DEF_LOG ("main", "optChannel: " << optBroceliande.optChannel);

  try {
    return createBroceliande (optBroceliande);
  } catch (...) {
    try {
      std::exception_ptr curr_excp;
      if (curr_excp = std::current_exception ())
	rethrow_exception (curr_excp);
    } catch (const std::exception& e) {
      cerr << endl << Log::getLocalTimeStr () << "Broceliande failed!" << e.what () << endl;
      return 1;
    }
    cerr << endl << Log::getLocalTimeStr () << "Broceliande failed! (Unknown exception)" << endl;
  }
}

int
obelix::broceliande::createBroceliande (OptBroceliande &optBroceliande) {
  switch (optBroceliande.inputImage.getDataType ()) {
  case GDT_Byte:
    return createBroceliandeB<uint8_t> (optBroceliande);
  case GDT_UInt16:
    return createBroceliandeB<uint16_t> (optBroceliande);
  case GDT_Int16:
    return createBroceliandeB<int16_t> (optBroceliande);
  case GDT_UInt32:
    return createBroceliandeB<uint32_t> (optBroceliande);
  case GDT_Int32:
    return createBroceliandeB<int32_t> (optBroceliande);
  case GDT_Float32:
    return createBroceliandeB<float> (optBroceliande);
  case GDT_Float64:
    return createBroceliandeB<double> (optBroceliande);
  default :
    cerr << "Unknown image pixel depth: " << GDALGetDataTypeName (optBroceliande.inputImage.getDataType ()) << endl << endl << flush;
    return 1;
  }
}

template<typename PixelT>
bool
obelix::broceliande::chooseGroundTruth (Broceliande<PixelT> &broceliande, IImage<GeoDimT> &groundTruthImage, const double &bgValue) {
  if (groundTruthImage.getDataType () != GDT_Byte)
    cerr << "*** Ground truth must be byte layer" << endl << flush;

  switch (groundTruthImage.getDataType ()) {
  case GDT_Byte:	return broceliande.chooseGroundTruth (groundTruthImage, uint8_t (bgValue));
  case GDT_UInt16:	return broceliande.chooseGroundTruth (groundTruthImage, uint16_t (bgValue));
  case GDT_Int16:	return broceliande.chooseGroundTruth (groundTruthImage, int16_t (bgValue));
  case GDT_UInt32:	return broceliande.chooseGroundTruth (groundTruthImage, uint32_t (bgValue));
  case GDT_Int32:	return broceliande.chooseGroundTruth (groundTruthImage, int32_t (bgValue));
  case GDT_Float32:	return broceliande.chooseGroundTruth (groundTruthImage, float (bgValue));
  case GDT_Float64:	return broceliande.chooseGroundTruth (groundTruthImage, double (bgValue));
  default :
    cerr << "Unknown ground truth pixel depth: " << GDALGetDataTypeName (groundTruthImage.getDataType ()) << endl << endl << flush;
    return false;
  }
}

// ================================================================================
template<typename PixelT>
int
obelix::broceliande::createBroceliandeB (OptBroceliande &optBroceliande) {
  DEF_LOG ("createBroceliandeB", "");

  RFClassifier<SharkLabelType> classifier; //XXX = RFClassifier<SharkLabelType> ();
  if (!optBroceliande.optTraining.loadTrainName.empty ()) {
    optBroceliande.optChannel.loadFile (optBroceliande.optTraining.loadChannelName);
    optBroceliande.optChannel.updateChannels ();
    // XXX dans optBroceliande verif loadTrainName compatible avec loadChannel
    // XXX ici vérif compatibilité des bandes img avec optChannel
    std::ifstream ifs (optBroceliande.optTraining.loadTrainName.c_str ());
    TextInArchive ia (ifs);
    classifier.load (ia, 0);
  }
  if (!optBroceliande.optChannel.getOutputCount ())
    throw invalid_argument ("No output band selection");

  if (!optBroceliande.inputImage.isEmpty ()) {
    Broceliande<PixelT> broceliande (optBroceliande.inputImage, optBroceliande.outputImage, optBroceliande.optCrop.topLeft, optBroceliande.optCrop.size, optBroceliande);
    broceliande.parseInput ();
    if (!optBroceliande.optGroundTruth.image.isEmpty ()) {
      if (!chooseGroundTruth (broceliande, optBroceliande.optGroundTruth.image, optBroceliande.optGroundTruth.bgValue))
	throw invalid_argument ("Can't choose groundTruth!");
      if (!broceliande.chooseSamples ())
	throw invalid_argument ("No samples!");
      broceliande.training (classifier);
      if (!optBroceliande.optTraining.saveTrainName.empty ()) {
	optBroceliande.optChannel.saveFile (optBroceliande.optTraining.saveChannelName);
	std::ofstream ofs (optBroceliande.optTraining.saveTrainName.c_str ());
	TextOutArchive oa (ofs);
	classifier.save (oa, 0);
	ofs.flush ();
	ofs.close ();
      }
      if (!optBroceliande.optGroundTruth.show.empty ())
	broceliande.showGroundTruth (optBroceliande.optGroundTruth.show, optBroceliande.optGroundTruth.image);
    }
    if (optBroceliande.optGroundTruth.image.isEmpty () &&
	optBroceliande.optTraining.loadTrainName.empty ())
      broceliande.snapshot ();
    else
      broceliande.predict (classifier);
    
    if (optBroceliande.countFlag)
      cout << TreeStats::global.printDim ();
    if (optBroceliande.timeFlag)
      cerr << TreeStats::global.printTime ();
  }
  // XXX sauver bandCount de inputType et libérer inputImage, groundTruthImage, outputImage
  if (!optBroceliande.optBatch.batchDir.empty ()) {
    path batchPath (optBroceliande.optBatch.batchDir);
    path resultPath (optBroceliande.optBatch.batchResultDir);
    for (auto i = directory_iterator (batchPath); i != directory_iterator (); ++i) {
      if (is_directory (i->path ()))
  	continue;
      path src (i->path ());
      path dst (resultPath / (optBroceliande.optBatch.batchPrefixResult + i->path ().filename ().string ()));
      IImage<GeoDimT> nextInputImage (src.string ());
      nextInputImage.readImage ();
      if (!nextInputImage.getBandCount ()) {
  	// déjà une trace avec GDAL
  	continue;
      }
      if (!optBroceliande.inputImage.isEmpty ()) {
  	if (optBroceliande.inputImage.getBandCount () != nextInputImage.getBandCount ()) {
	  cerr << "Skip bad count band " << nextInputImage  << endl;
  	  continue;
	}
  	if (optBroceliande.inputImage.getDataType () != nextInputImage.getDataType ()) {
	  cerr << "Skip bad data type " << nextInputImage  << endl;
	  continue;
	}
      } else {
  	// XXX devrait vérifier la compatibilité des bandes chargées
      }
      if (exists (dst)) {
  	if (optBroceliande.optBatch.batchOverwriteFlag)
  	  cout << Log::getLocalTimeStr () << " overwrite " << dst.string () << endl;
  	else {
  	  cout << Log::getLocalTimeStr () << " skip " << dst.string () << endl;
  	  continue;
  	}
      }
      try {
  	cout << Log::getLocalTimeStr () << " Batch from: " << src.string () << " to: " << dst.string () << endl;
  	// XXX test déjà produit
	TreeStats::global.reset ();
  	IImage<GeoDimT> outputImage (dst.string ());
  	Broceliande<PixelT> broceliande (nextInputImage, outputImage, NullPoint2D, nextInputImage.getSize (), optBroceliande);
  	broceliande.parseInput ();
  	broceliande.predict (classifier);

	if (optBroceliande.timeFlag)
	  cerr << TreeStats::global.printDim () << endl
	       << TreeStats::global.printTime ();
      } catch (...) {
  	cout << endl << Log::getLocalTimeStr () << " Production from: " << src.string () << " to: " << dst.string () << " failed!" << endl;
      }
    }
  }
  return 0;
}

// ========================================
