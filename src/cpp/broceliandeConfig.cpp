////////////////////////////////////////////////////////////////////////////
// Copyright IRISA 2017							  //
// 									  //
// triskele.obelix (at) irisa.fr					  //
// 									  //
// This  software is  a computer  program  whose purpose  is to  classify //
// pixels by using TRISKELE and random forest.				  //
// 									  //
// This software is governed by the CeCILL-B license under French law and //
// abiding by  the rules of distribution  of free software. You  can use, //
// modify  and/or  redistribute  the  software under  the  terms  of  the //
// CeCILL-B license as circulated by CEA, CNRS and INRIA at the following //
// URL "http://www.cecill.info".					  //
// 									  //
// As a counterpart to the access to  the source code and rights to copy, //
// modify and  redistribute granted  by the  license, users  are provided //
// only with a limited warranty and  the software's author, the holder of //
// the economic  rights, and the  successive licensors have  only limited //
// liability.								  //
// 									  //
// In this respect, the user's attention is drawn to the risks associated //
// with loading,  using, modifying  and/or developing or  reproducing the //
// software by the user in light of its specific status of free software, //
// that may  mean that  it is  complicated to  manipulate, and  that also //
// therefore means  that it  is reserved  for developers  and experienced //
// professionals having in-depth computer  knowledge. Users are therefore //
// encouraged  to load  and test  the software's  suitability as  regards //
// their  requirements  in  conditions  enabling the  security  of  their //
// systems and/or  data to  be ensured  and, more  generally, to  use and //
// operate it in the same conditions as regards security.		  //
// 									  //
// The fact that  you are presently reading this means  that you have had //
// knowledge of the CeCILL-B license and that you accept its terms.	  //
////////////////////////////////////////////////////////////////////////////

#include <iostream>
#include <iterator>

#include <boost/program_options.hpp>

using namespace std;
using namespace boost;
using namespace boost::program_options;

#include "obelixDebug.hpp"
#include "IImage.hpp"

#include "misc.hpp"
#include "Appli/OptChannel.hpp"
#include "Appli/OptChannelParser.hpp"

using namespace obelix;
using namespace obelix::triskele;

namespace po = boost::program_options;

// ================================================================================
static OptChannelParser optChannelParser;
static po::options_description mainDescription ("Main options", getCols ());
static po::options_description hide ("Hidded options", getCols ());
static char *prog = NULL;

// ================================================================================
void
usage (const string &msg = "", const bool &hidden = false) {
  if (!msg.empty ())
    cout << msg << endl;
  cout << endl
       <<"Usage: " << endl
       <<"       " << prog << " [options] [inputImage]" << endl
       << endl <<"  create configuration for Broceliande" << endl
       << endl << mainDescription
       << endl << optChannelParser.description
       << endl;
  exit (1);
  if (hidden)
    cout << hide << endl;
  exit (1);
}

// ================================================================================
static const string inputFile = "input-file";
static const char *const inputFileC = inputFile.c_str ();

// ================================================================================
int
main (int argc, char** argv) {
  // debug = true;
  prog = argv [0];
  try {

    bool helpFlag (false), debugFlag = false, useTheForceLuke = false;
    string inputName;

    mainDescription.add_options ()
      ("help", po::bool_switch (&helpFlag), "produce this help message")
      ("input,i", po::value<string> (&inputName), "input file name image (to determine the number bands)")
      ;

    hide.add_options ()
      ("useTheForceLuke", po::bool_switch (&useTheForceLuke), "display hidded options")
      ("debug", po::bool_switch (&debugFlag), "debug mode")
      ;

    po::options_description cmd ("All options");
    cmd.add (mainDescription).add (optChannelParser.description).add (hide).add_options ()
      (inputFileC, po::value<vector<string> > (), "input output")
      ;

    po::positional_options_description p;
    p.add (inputFileC, -1);
    po::variables_map vm;
    po::basic_parsed_options<char> parsed = po::command_line_parser (argc, argv).options (cmd).positional (p).run ();
    store (parsed, vm);
    po::notify (vm);

    if (debugFlag) {
#ifndef ENABLE_LOG
      cout << "No debug option available (was compiled with -DENABLE_LOG)" << endl;
#endif
    }
    Log::debug = debugFlag;
    if (useTheForceLuke)
      usage ("", true);
    if (helpFlag)
      usage ();

    if (vm.count (inputFileC)) {
      vector<string> var = vm[inputFileC].as<vector<string> > ();
      int nbArgs = vm[inputFileC].as<vector<string> > ().size ();
      int setArgs = 0;
      if (inputName.empty ())
	inputName = var [setArgs++];
      if (nbArgs > setArgs)
	usage ("Too much arguments");
    }

    OptChannel optChannel;
    optChannelParser.parse (optChannel, inputName, parsed);

    if (debugFlag)
      cout << optChannel;

    return 0;
  } catch (std::exception& e) {
    cerr << "error: " << e.what() << endl;
    usage ();
    return 1;
  } catch (...) {
    cerr << "Exception of unknown type!" << endl;
    return 1;
  }
}

// ================================================================================
