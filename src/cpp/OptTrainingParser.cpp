////////////////////////////////////////////////////////////////////////////
// Copyright IRISA 2017							  //
// 									  //
// triskele.obelix (at) irisa.fr					  //
// 									  //
// This  software is  a computer  program  whose purpose  is to  classify //
// pixels by using TRISKELE and random forest.				  //
// 									  //
// This software is governed by the CeCILL-B license under French law and //
// abiding by  the rules of distribution  of free software. You  can use, //
// modify  and/or  redistribute  the  software under  the  terms  of  the //
// CeCILL-B license as circulated by CEA, CNRS and INRIA at the following //
// URL "http://www.cecill.info".					  //
// 									  //
// As a counterpart to the access to  the source code and rights to copy, //
// modify and  redistribute granted  by the  license, users  are provided //
// only with a limited warranty and  the software's author, the holder of //
// the economic  rights, and the  successive licensors have  only limited //
// liability.								  //
// 									  //
// In this respect, the user's attention is drawn to the risks associated //
// with loading,  using, modifying  and/or developing or  reproducing the //
// software by the user in light of its specific status of free software, //
// that may  mean that  it is  complicated to  manipulate, and  that also //
// therefore means  that it  is reserved  for developers  and experienced //
// professionals having in-depth computer  knowledge. Users are therefore //
// encouraged  to load  and test  the software's  suitability as  regards //
// their  requirements  in  conditions  enabling the  security  of  their //
// systems and/or  data to  be ensured  and, more  generally, to  use and //
// operate it in the same conditions as regards security.		  //
// 									  //
// The fact that  you are presently reading this means  that you have had //
// knowledge of the CeCILL-B license and that you accept its terms.	  //
////////////////////////////////////////////////////////////////////////////

#include <stdexcept>
#include <boost/program_options.hpp>

#include "obelixDebug.hpp"
#include "misc.hpp"

#include "OptTrainingParser.hpp"

using namespace std;
using namespace boost;
using namespace boost::program_options;

using namespace obelix::broceliande;

namespace po = boost::program_options;

// ================================================================================

OptTrainingParser::OptTrainingParser ()
  : cleanTagsFlag (false),
    cleanSamplesFlag (false),
    nTrees (64), // => 256 values
    maxPerRFBatch (1<<16),
    description ("Training options", getCols ()) {
    DEF_LOG ("OptTrainingParser::OptTrainingParser", "");

    description.add_options ()
      ("cleanTagsFlag", po::bool_switch (&cleanTagsFlag), "clean all ground truth tagged classes")
      ("cleanSamplesFlag", po::bool_switch (&cleanSamplesFlag), "clean samples for training")

      ("nTrees", po::value<unsigned int> (&nTrees)->default_value (nTrees), "number of trees for random forest algorithm")
      ("maxPerRFBatch", po::value<DimImg> (&maxPerRFBatch)->default_value (maxPerRFBatch), "size of batch for random forest algorithm")

      ("loadTrain", po::value<string> (&loadTrainName), "load training")
      ("saveTrain", po::value<string> (&saveTrainName), "save training")
      ;
}

// ================================================================================
void
OptTrainingParser::parse (OptTraining &optTraining, po::basic_parsed_options<char> &parsed) {
  DEF_LOG ("OptTrainingParser::parse", "");

  if (!loadTrainName.empty ()) {
    loadTrainName = addExtensionIfNone (loadTrainName, OptTraining::rfExt);
    optTraining.loadChannelName = replaceExtension (loadTrainName, OptTraining::rfChExt);
  }
  if (!saveTrainName.empty ()) {
    saveTrainName = addExtensionIfNone (saveTrainName, OptTraining::rfExt);
    optTraining.saveChannelName = replaceExtension (saveTrainName, OptTraining::rfChExt);
  }

  optTraining.cleanTagsFlag = cleanTagsFlag;
  optTraining.cleanSamplesFlag = cleanSamplesFlag;
  optTraining.nTrees = nTrees;
  optTraining.maxPerRFBatch = maxPerRFBatch;
  optTraining.loadTrainName = loadTrainName;
  optTraining.saveTrainName = saveTrainName;
}

// ================================================================================
