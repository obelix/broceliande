////////////////////////////////////////////////////////////////////////////
// Copyright IRISA 2017							  //
// 									  //
// triskele.obelix (at) irisa.fr					  //
// 									  //
// This  software is  a computer  program  whose purpose  is to  classify //
// pixels by using TRISKELE and random forest.				  //
// 									  //
// This software is governed by the CeCILL-B license under French law and //
// abiding by  the rules of distribution  of free software. You  can use, //
// modify  and/or  redistribute  the  software under  the  terms  of  the //
// CeCILL-B license as circulated by CEA, CNRS and INRIA at the following //
// URL "http://www.cecill.info".					  //
// 									  //
// As a counterpart to the access to  the source code and rights to copy, //
// modify and  redistribute granted  by the  license, users  are provided //
// only with a limited warranty and  the software's author, the holder of //
// the economic  rights, and the  successive licensors have  only limited //
// liability.								  //
// 									  //
// In this respect, the user's attention is drawn to the risks associated //
// with loading,  using, modifying  and/or developing or  reproducing the //
// software by the user in light of its specific status of free software, //
// that may  mean that  it is  complicated to  manipulate, and  that also //
// therefore means  that it  is reserved  for developers  and experienced //
// professionals having in-depth computer  knowledge. Users are therefore //
// encouraged  to load  and test  the software's  suitability as  regards //
// their  requirements  in  conditions  enabling the  security  of  their //
// systems and/or  data to  be ensured  and, more  generally, to  use and //
// operate it in the same conditions as regards security.		  //
// 									  //
// The fact that  you are presently reading this means  that you have had //
// knowledge of the CeCILL-B license and that you accept its terms.	  //
////////////////////////////////////////////////////////////////////////////

// ----------------------------------------
template<typename PixelT, typename PixelGT>
void
ObelixDap<PixelT, PixelGT>::cleanFgTag () {
  if (!options.cleanFgTag || !options.cleanFgRate.getValue ())
    return;
  DEF_LOG ("ObelixDap::cleanFgTag", "pixelCount: " << pixelCount);
  auto start = high_resolution_clock::now ();

  chooseFg1 (Rate ("100%"));
  chooseBgRef (Rate ("100%"), Rate ("100%"));
  RFTrainer<unsigned int> trainer (false, false);
  RFClassifier<unsigned int> classifierForCleanFgTag;
  ClassificationDataset trainingData;
  trainer.setNTrees (options.nTrees);
  trainer.setMTry (sqrt (apMap.getBandCount ()+textureMap.getBandCount ()));

  getSamples (trainingData, GroundTruth::sampleSet, noCandidats);
  trainer.train (classifierForCleanFgTag, trainingData);

  ClassificationDataset testingData;
  getSamples (testingData, GroundTruth::fgTagSet, noCandidats);
  Data<RealVector> predictionTest = classifierForCleanFgTag (testingData.inputs ());
  DimImg fgTagCount = groundTruth.getCount (GroundTruth::fgTagSet);
  vector<DimImg> badFg;
  badFg.reserve (fgTagCount*0.1);
  double lessAccuracy = options.cleanFgRate.getValue ();
  for (DimImg predIdx = 0; predIdx < fgTagCount; ++predIdx)
    if (predictionTest.element (predIdx)[1] < lessAccuracy)
      badFg.push_back (predIdx);
  cout << getLocalTimeStr () << " remove badFgTag: " << badFg.size () << endl;
  groundTruth.cleanFgTag (badFg);
  timeStats[cleanStats] (duration_cast<duration<double> > (high_resolution_clock::now ()-start).count ());
}
// ----------------------------------------
template<typename PixelT, typename PixelGT>
void
ObelixDap<PixelT, PixelGT>::chooseBgRef (const Rate &bgRefRate, const Rate &bgTagRate) {
  DEF_LOG ("ObelixDap::chooseBgRef",
	   "pixelCount: " << pixelCount << " bgRefRate:" << bgRefRate << " bgTagRate:" << bgTagRate << " bgExtraRate:" << options.bgExtraRate);
  if (!options.bgExtraRate.getValue ()) {
    groundTruth.chooseBgRef (bgRefRate, bgTagRate);
    return;
  }

  vector<double> fgAverage (profileMap.bandOutputSize, 0.);
  groundTruth.forEachIdx (GroundTruth::fg1ISet,
			  [this, &fgAverage] (const DimImg &idxSet, const bool &isFg, const DimImg &idxRed, const DimImg &idxReal) {
			    const vector<PixelT> &bands = allProfiles[useGT ? idxRed : idxReal];
			    for (DimChannel i = 0; i < profileMap.bandOutputSize; ++i)
			      fgAverage [i] += bands[i];
			  });
  for (DimChannel i = 0; i < profileMap.bandOutputSize; ++i)
    fgAverage [i] /= groundTruth.getCount (GroundTruth::fg1DiscSet);

  groundTruth.chooseBgRef (bgRefRate, bgTagRate,
			   [this, &fgAverage] (const DimImg &bgRndRedI, const DimImg &bgRndRealI) {
			     const double *val1 = fgAverage.data ();
			     const PixelT *val2 = allProfiles[useGT ? bgRndRedI : bgRndRealI].data ();
			     double dist = 0;
			     for (DimChannel i = 0; i < profileMap.bandOutputSize; ++i) {
			       double dif = *val1-- - *val2--;
			       dist += dif*dif;
			     }
			     // return sqrt (dist); don't use sqrt ofr only order
			     return dist;
			   }, options.bgExtraRate, options.farBgRate);
}
// ----------------------------------------
template<typename PixelT, typename PixelGT>
void
ObelixDap<PixelT, PixelGT>::cleanFg1 (const Rate &cleanFg1Rate) {
  if (options.cleanFgTag || !cleanFg1Rate.getValue ())
    return;
  DEF_LOG ("ObelixDap::cleanFg1", "");
  chooseBgRef (options.bgRefRate, options.bgTagRate);
  cleanBgRef (Rate ("50%"));
  auto start = high_resolution_clock::now ();

  RFTrainer<unsigned int> trainer (false, false);
  RFClassifier<unsigned int> classifierForClean;
  ClassificationDataset trainingData;
  trainer.setNTrees (options.nTrees);
  trainer.setMTry (sqrt (apMap.getBandCount ()+textureMap.getBandCount ()));
  getSamples (trainingData, GroundTruth::sampleSet, noCandidats);
  trainer.train (classifierForClean, trainingData);

  ClassificationDataset testingData;
  getSamples (testingData, GroundTruth::fg1ISet, noCandidats);
  Data<RealVector> predictionTest = classifierForClean (testingData.inputs ());
  vector<DimImg> badFg1;
  DimImg fg1Count = groundTruth.getCount (GroundTruth::fg1ISet);
  badFg1.reserve (fg1Count*0.2);
  double minAccuracy = cleanFg1Rate.getValue ();
  for (DimImg predIdx = 0; predIdx < fg1Count; ++predIdx)
    if (predictionTest.element (predIdx)[1] < minAccuracy)
      badFg1.push_back (predIdx);
  cout << getLocalTimeStr () << " remove badFg1: " << badFg1.size () << endl;
  groundTruth.cleanFg1 (badFg1);
  timeStats[cleanStats] (duration_cast<duration<double> > (high_resolution_clock::now ()-start).count ());
}
// ----------------------------------------
template<typename PixelT, typename PixelGT>
void
ObelixDap<PixelT, PixelGT>::cleanBgRef (const Rate &cleanBgRate) {
  if (!cleanBgRate.getValue ())
    return;
  DEF_LOG ("ObelixDap::cleanBgRef", "");
  auto start = high_resolution_clock::now ();

  RFTrainer<unsigned int> trainer (false, false);
  RFClassifier<unsigned int> classifierForClean;
  ClassificationDataset trainingData;
  trainer.setNTrees (options.nTrees);
  trainer.setMTry (sqrt (apMap.getBandCount ()+textureMap.getBandCount ()));
  getSamples (trainingData, GroundTruth::sampleSet, noCandidats);
  trainer.train (classifierForClean, trainingData);

  ClassificationDataset testingData;
  getSamples (testingData, GroundTruth::bgRefSet, noCandidats);
  Data<RealVector> predictionTest = classifierForClean (testingData.inputs ());
  vector<DimImg> badBgRef;
  DimImg bgRefCount = groundTruth.getCount (GroundTruth::bgRefSet);
  badBgRef.reserve (bgRefCount*0.2);
  double minAccuracy = cleanBgRate.getValue ();
  for (DimImg predIdx = 0; predIdx < bgRefCount; ++predIdx)
    if (predictionTest.element (predIdx)[0] < minAccuracy)
      badBgRef.push_back (predIdx);
  cout << getLocalTimeStr () << " remove badBgRef: " << badBgRef.size () << endl;
  groundTruth.cleanBgRef (badBgRef);
  timeStats[cleanStats] (duration_cast<duration<double> > (high_resolution_clock::now ()-start).count ());
  LOG ("bg: " << groundTruth.getCount (GroundTruth::bgTagSet) << " (+ " << groundTruth.getCount (GroundTruth::bgRndSet) << ") " <<
       " bgRef: " << groundTruth.getCount (GroundTruth::bgRefSet));
}
