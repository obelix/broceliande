////////////////////////////////////////////////////////////////////////////
// Copyright IRISA 2017							  //
// 									  //
// triskele.obelix (at) irisa.fr					  //
// 									  //
// This  software is  a computer  program  whose purpose  is to  classify //
// pixels by using TRISKELE and random forest.				  //
// 									  //
// This software is governed by the CeCILL-B license under French law and //
// abiding by  the rules of distribution  of free software. You  can use, //
// modify  and/or  redistribute  the  software under  the  terms  of  the //
// CeCILL-B license as circulated by CEA, CNRS and INRIA at the following //
// URL "http://www.cecill.info".					  //
// 									  //
// As a counterpart to the access to  the source code and rights to copy, //
// modify and  redistribute granted  by the  license, users  are provided //
// only with a limited warranty and  the software's author, the holder of //
// the economic  rights, and the  successive licensors have  only limited //
// liability.								  //
// 									  //
// In this respect, the user's attention is drawn to the risks associated //
// with loading,  using, modifying  and/or developing or  reproducing the //
// software by the user in light of its specific status of free software, //
// that may  mean that  it is  complicated to  manipulate, and  that also //
// therefore means  that it  is reserved  for developers  and experienced //
// professionals having in-depth computer  knowledge. Users are therefore //
// encouraged  to load  and test  the software's  suitability as  regards //
// their  requirements  in  conditions  enabling the  security  of  their //
// systems and/or  data to  be ensured  and, more  generally, to  use and //
// operate it in the same conditions as regards security.		  //
// 									  //
// The fact that  you are presently reading this means  that you have had //
// knowledge of the CeCILL-B license and that you accept its terms.	  //
////////////////////////////////////////////////////////////////////////////

#include <algorithm>

#include "GroundTruth.hpp"
#include "IImage.hpp"
#include <algorithm>    // std::transform

using namespace obelix;
using namespace obelix::broceliande;

// ================================================================================
void
GroundTruth::createSequence (vector<DimImg> &tmpIdx, const DimImg &max) {
  size_t size (max);
  tmpIdx.resize (size);
  if (tmpIdx.empty ())
    return;
  for (DimImg i = 0; i < max; ++i)
    tmpIdx [i] = i;
  random_shuffle (tmpIdx.begin (), tmpIdx.end ());
}

void
GroundTruth::randIdx (vector<DimImg> &tmpIdx, const DimImg &max, const DimImg &count) {
  BOOST_ASSERT (count <= max);
  if (!count) {
    tmpIdx.clear ();
    //vector<DimImg>().swap (tmpIdx);
    return;
  }
  if (max == count) {
    createSequence (tmpIdx, max);
    return;
  }
  vector<DimImg> tmp2Idx (max);
  createSequence (tmp2Idx, max);
  tmpIdx.assign (tmp2Idx.begin (), tmp2Idx.begin ()+count);
  sort (tmpIdx.begin (), tmpIdx.end ());
}

void
GroundTruth::resetTagsPos () {
  DEF_LOG ("GroundTruth::resetTagsPos", "");
  BroceliandeLabelType classCount (tagsIdx.size ());
  LOG ("classCount: " << classCount);
  tagsPos.resize (classCount+1);
  tagsPos[0] = bgRndIdx.size ();
  LOG ("tagsPos[0]: " << tagsPos[0]);
  for (BroceliandeLabelType classIdx = 0; classIdx < classCount; ++classIdx) {
    tagsPos [classIdx+1] = tagsPos [classIdx]+tagsIdx[classIdx].size ();
    LOG ("tagsPos[" << (classIdx+1)<< "]: " << tagsPos[classIdx+1]);
  }
}

void
GroundTruth::resetSamplesPos () {
  DEF_LOG ("GroundTruth::resetSamplesPos", "");
  BroceliandeLabelType classCount (tagsIdxIdx.size ());
  LOG ("classCount: " << classCount);
  samplesPos.resize (classCount+1);
  samplesPos[0] = bgRndIdxIdx.size ();
  LOG ("samplesPos[0]: " << samplesPos[0]);
  for (BroceliandeLabelType classIdx = 0; classIdx < classCount; ++classIdx) {
    samplesPos [classIdx+1] = samplesPos [classIdx]+tagsIdxIdx[classIdx].size ();
    LOG ("samplesPos[" << (classIdx+1)<< "]: " << samplesPos[classIdx+1]);
  }
}

// ================================================================================
GroundTruth::GroundTruth (const size_t &coreCount)
  : coreCount (coreCount) {
  DEF_LOG ("GroundTruth::GroundTruth", "");
}

GroundTruth::~GroundTruth () {
  DEF_LOG ("GroundTruth::~GroundTruth", "");
}

// ================================================================================
void
GroundTruth::chooseSamples (const OptRate &tagsRate, const OptRate &bgRate, const OptRate &bgTagRate) {
  DEF_LOG ("GroundTruth::chooseSamples", "tagsRate: " << tagsRate);
  DimImg maxTagCount (0);
  LOG ("classCount: " << tagsIdxIdx.size ());
  tagsIdxIdx.resize (tagsIdx.size ());
  for (BroceliandeLabelType classIdx = 1; classIdx < tagsIdx.size (); ++classIdx) {
    const DimImg classSize (tagsIdx[classIdx].size ());
    const DimImg maxCount (min (classSize, tagsRate.getRate (classSize)));
    maxTagCount = max (maxTagCount, classSize);
    randIdx (tagsIdxIdx[classIdx], classSize, maxCount);
    // cout << "coucou tagsIdxIdx[" << classIdx << "]: " << tagsIdxIdx[classIdx].size () << endl;
    LOG ("tagsIdxIdx[" << classIdx << "]: " << tagsIdxIdx[classIdx].size ());
  }

  DimImg bgTagCount (tagsIdx[0].size ());
  DimImg bgRndCount = bgRndIdx.size ();
  DimImg bgCount = min (bgTagCount+bgRndCount, bgRate.getRate (maxTagCount));
  DimImg bgTagIdxIdxCount = min (bgTagCount, min (bgCount, bgTagRate.getRate (bgCount)));
  DimImg bgRndIdxIdxCount = min (bgRndCount, bgCount - bgTagIdxIdxCount);

  randIdx (tagsIdxIdx[0], bgTagCount, bgTagIdxIdxCount);
  LOG ("tagsIdxIdx[0]: " << tagsIdxIdx[0].size ());
  randIdx (bgRndIdxIdx, bgRndCount, bgRndIdxIdxCount);
  LOG ("bgRndIdxIdx[0]: " << bgRndIdxIdx.size ());
  LOG ("classCount: " << tagsIdxIdx.size ());

  resetSamplesPos ();
}

// ================================================================================
void
GroundTruth::cleanTag (const vector<DimImg> &sortedBadTag) {
  vector<DimImg> tmpTagsPos (tagsPos);
  vector<DimImg>::const_iterator low = sortedBadTag.begin ();
  vector<DimImg>::const_iterator up = lower_bound (low, sortedBadTag.end (), bgRndIdx.size ());
  if (low != up) {
    vector<DimImg> mvIdx (low, up);
    removeInVector (bgRndIdx, mvIdx, true);
  }
  low = up;
  BroceliandeLabelType classCount = tagsIdx.size ();
  for (BroceliandeLabelType classIdx = 0; classIdx < classCount; ++classIdx) {
    up = lower_bound (low, sortedBadTag.end (), tmpTagsPos[classIdx]+tagsIdx[classIdx].size ());
    if (low == up)
      continue;
    vector<DimImg> mvIdx (up-low);
    const DimImg pos = tmpTagsPos[classIdx];
    transform (low, up, mvIdx.begin (), [&pos] (const DimImg &v) { return v-pos; });
    removeInVector (tagsIdx[classIdx], mvIdx, true);
    low = up;
  }
  resetTagsPos ();
  // XXX clear samplesPos
}

void
GroundTruth::cleanSamples (const vector<DimImg> &sortedBadSamples) {
  vector<DimImg> tmpSamplesPos (samplesPos);
  vector<DimImg> tmpTagsPos (tagsPos);
  vector<DimImg>::const_iterator low = sortedBadSamples.begin ();
  vector<DimImg>::const_iterator up = lower_bound (low, sortedBadSamples.end (), bgRndIdx.size ());
  if (low != up) {
    vector<DimImg> mvIdx (low, up);
    removeInVector (bgRndIdx, mvIdx, true);
    transform (mvIdx.begin (), mvIdx.end (), mvIdx.begin (), [this] (const DimImg &v) { return bgRndIdxIdx [v]; });
    removeInIndirectVector (bgRndIdxIdx, mvIdx);
  }
  low = up;
  BroceliandeLabelType classCount = tagsIdx.size ();
  for (BroceliandeLabelType classIdx = 0; classIdx < classCount; ++classIdx) {
    up = lower_bound (low, sortedBadSamples.end (), tmpTagsPos[classIdx]+tagsIdx[classIdx].size ());
    if (low == up)
      continue;
    vector<DimImg> mvIdx (up-low);
    const DimImg pos = tmpTagsPos[classIdx];
    transform (low, up, mvIdx.begin (), [&pos] (const DimImg &v) { return v-pos; });
    removeInVector (tagsIdx[classIdx], mvIdx, true);
    vector<DimImg> &idxIdx (tagsIdxIdx[classIdx]);
    transform (mvIdx.begin (), mvIdx.end (), mvIdx.begin (), [&idxIdx] (const DimImg &v) { return idxIdx [v]; });
    removeInIndirectVector (idxIdx, mvIdx);
    low = up;
  }
  resetTagsPos ();
  resetSamplesPos ();
}


// ================================================================================
void
GroundTruth::showGroundTruth (const Border<2> &border, const string &filename, const IImage<2> &inputImage, const Point<2> &topLeft) const {
  typedef uint8_t PixelD;
  static const PixelD HV (numeric_limits<PixelD>::max ());
  static const PixelD MV (MV/4*3);
  static const PixelD LV (MV/2);
  DEF_LOG ("GroundTruth::showGroundTruth", "filename: " << filename);

  // |-------+--------+----------+-----------+-------------+------------+------------+----------------|
  // | color | Border | bg       | bg sample | rnd         | rnd sample | class i    | class i sample |
  // | band  | gray   | half red | red       | half orange | orange     | green/blue | green          |
  // |-------+--------+----------+-----------+-------------+------------+------------+----------------|
  // |       |        |          |           |             |            |            |                |
  // | R     | LV     | MV       | HV        | MV          | HV         | 0          | 0              |
  // | G     | LV     | 0        | 0         | LV          | MV         | 0          | ci             |
  // | B     | LV     | 0        | 0         | O           | 0          | ci         | 0              |
  // |       |        |          |           |             |            |            |                |
  // |-------+--------+----------+-----------+-------------+------------+------------+----------------|


  BroceliandeLabelType classCount (tagsIdx.size () - 1);
  const Size<2> &size (border.getSize ());
  const DimImg pixelCount (size.getPixelsCount ());
  PixelD startV ((HV-classCount)/2);
  PixelD stepV ((HV-startV)/classCount);
  
  vector <PixelD> ci (classCount);
  for (PixelD i (0), v (startV); i < classCount; ++i, v += stepV)
    ci[i] = v;
  
  Raster<PixelD, 2> redRaster (size, PixelD (0));
  Raster<PixelD, 2> greenRaster (size, PixelD (0));
  Raster<PixelD, 2> blueRaster (size, PixelD (0));
  PixelD *redPixels = redRaster.getPixels ();
  PixelD *greenPixels = greenRaster.getPixels ();
  PixelD *bluePixels = blueRaster.getPixels ();

  for (DimImg pixelId = 0; pixelId < pixelCount; ++pixelId)
    if (border.isBorder (pixelId))
      redPixels[pixelId] = greenPixels[pixelId] = bluePixels[pixelId] = LV;

  for (DimImg p : tagsIdx[0])
    redPixels[p] = MV;
  for (DimImg pi : tagsIdxIdx[0])
    redPixels[tagsIdx[0][pi]] = HV;

  for (DimImg p : bgRndIdx) {
    redPixels[p] = MV;
    greenPixels[p] = LV;
  }
  for (DimImg pi : bgRndIdxIdx) {
    redPixels[bgRndIdx [pi]] = HV;
    greenPixels[bgRndIdx [pi]] = MV;
  }
  for (PixelD c (0); c < classCount; ++c) {
    const PixelD c2 (c+1);
    for (DimImg p : tagsIdx[c2])
      bluePixels[p] = ci [c];
    for (DimImg pi : tagsIdxIdx[c2]) {
      bluePixels[tagsIdx [c2][pi]] = 0;
      greenPixels[tagsIdx [c2][pi]] = ci [c];
    }
  }

  IImage<2> selectedGroundTruthImage (filename);
  selectedGroundTruthImage.createImage (size, GDT_Byte, 3, inputImage, topLeft);
  selectedGroundTruthImage.writeBand (redPixels, 0);
  selectedGroundTruthImage.writeBand (greenPixels, 1);
  selectedGroundTruthImage.writeBand (bluePixels, 2);
  selectedGroundTruthImage.close ();
}

// ================================================================================
