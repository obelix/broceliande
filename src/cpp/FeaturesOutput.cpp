////////////////////////////////////////////////////////////////////////////
// Copyright IRISA 2017							  //
// 									  //
// triskele.obelix (at) irisa.fr					  //
// 									  //
// This  software is  a computer  program  whose purpose  is to  classify //
// pixels by using TRISKELE and random forest.				  //
// 									  //
// This software is governed by the CeCILL-B license under French law and //
// abiding by  the rules of distribution  of free software. You  can use, //
// modify  and/or  redistribute  the  software under  the  terms  of  the //
// CeCILL-B license as circulated by CEA, CNRS and INRIA at the following //
// URL "http://www.cecill.info".					  //
// 									  //
// As a counterpart to the access to  the source code and rights to copy, //
// modify and  redistribute granted  by the  license, users  are provided //
// only with a limited warranty and  the software's author, the holder of //
// the economic  rights, and the  successive licensors have  only limited //
// liability.								  //
// 									  //
// In this respect, the user's attention is drawn to the risks associated //
// with loading,  using, modifying  and/or developing or  reproducing the //
// software by the user in light of its specific status of free software, //
// that may  mean that  it is  complicated to  manipulate, and  that also //
// therefore means  that it  is reserved  for developers  and experienced //
// professionals having in-depth computer  knowledge. Users are therefore //
// encouraged  to load  and test  the software's  suitability as  regards //
// their  requirements  in  conditions  enabling the  security  of  their //
// systems and/or  data to  be ensured  and, more  generally, to  use and //
// operate it in the same conditions as regards security.		  //
// 									  //
// The fact that  you are presently reading this means  that you have had //
// knowledge of the CeCILL-B license and that you accept its terms.	  //
////////////////////////////////////////////////////////////////////////////

#include "Broceliande.hpp"
#include "FeaturesOutput.hpp"

using namespace shark;
using namespace std;
using namespace obelix;
using namespace obelix::broceliande;
#include "FeaturesOutput.tcpp"

// ================================================================================
template <typename FeatureT>
template <typename PixelT>
void
FeatureOutput<FeatureT>::init (const FeaturesOutput<PixelT> &featuresOutput, const FeatureOutputType &featureOutputType) {
  const Size<2> size (featuresOutput.broceliande.getSize ());
  const DimImg pixelCount (featuresOutput.broceliande.getPixelCount ());
  const OptChannel &optChannel (featuresOutput.broceliande.optChannel);

  this->featureOutputType = featureOutputType;
  localIdx.resize (optChannel.getMaxInputIdCount (), 0);

  if (featureOutputType == PixelFeature) {
    orig4Copy.reserve (optChannel.getCopyCount ());
    // } if (featureOutputType == DoubleFeature) {
    ndviRasters.reserve (optChannel.getNdviCount ());
    pantexRasters.reserve (optChannel.getPantexCount ());
    sobelRasters.reserve (optChannel.getSobelCount ());
  }

  for (const InputChannel *inputChannelP : optChannel.getActiveOrder ()) {
    const InputChannel &inputChannel (*inputChannelP);
    BOOST_ASSERT (inputChannel.prodWrite);
    if (featuresOutput.channelsFeaturesOutputType [inputChannel.inputId] != featureOutputType)
      continue;
    switch (inputChannel.channelType) {
    case Original:
      if (inputChannel.doWrite) {
	DimChannel copyCount (orig4Copy.size ());
	orig4Copy.push_back (vector<FeatureT> (pixelCount));
	localIdx[inputChannel.inputId] = copyCount;
	copySrcData.push_back (orig4Copy [copyCount].data ());
      } else if (inputChannel.prodWrite && inputChannel.prodDap) {
	DimChannel orig4DapCount (orig4Dap.size ());
	orig4Dap.push_back (vector<FeatureT> (pixelCount));
	localIdx[inputChannel.inputId] = orig4DapCount;
      }
      break;

    case Ndvi: {
      DimChannel ndviCount (ndviRasters.size ());
      ndviRasters.push_back (Raster<FeatureT, 2> (size));
      localIdx[inputChannel.inputId] = ndviCount;
      if (inputChannel.doWrite)
	copySrcData.push_back (ndviRasters [ndviCount].getPixelsVector ().data ());
      break;
    }

    case Pantex: {
      DimChannel pantexCount (pantexRasters.size ());
      pantexRasters.push_back (Raster<FeatureT, 2> (size));
      localIdx[inputChannel.inputId] = pantexCount;
      if (inputChannel.doWrite)
	copySrcData.push_back (pantexRasters [pantexCount].getPixelsVector ().data ());
      break;
    }

    case Sobel: {
      DimChannel sobelCount (sobelRasters.size ());
      sobelRasters.push_back (Raster<FeatureT, 2> (size));
      localIdx[inputChannel.inputId] = sobelCount;
      if (inputChannel.doWrite)
	copySrcData.push_back (sobelRasters [sobelCount].getPixelsVector ().data ());
      break;
    }

    case AP: {
      DimChannel apCount (apOutput.size ());
      apOutput.resize (apCount+1);
      vector<vector<FeatureT> > tmp (inputChannel.thresholds.size (), vector<FeatureT> (pixelCount));
      apOutput [apCount].swap (tmp);
      localIdx[inputChannel.inputId] = apCount;
      if (inputChannel.doWrite) {
	vector<vector<FeatureT> > &apSrc (apOutput[apCount]);
	for (DimChannel i = 0; i < apSrc.size (); ++i)
	  copySrcData.push_back (apSrc [i].data ());
      }
      break;
    }

    case DAP: {
      DimChannel dapCount (dapSrcIdx.size ());
      vector<vector<FeatureT> > &apSrc (apOutput[localIdx[inputChannel.generateBy[0]->inputId]]);
      dapSrcIdx.resize (dapCount+1);
      vector<FeatureT*> tmp (apSrc.size ()+1, nullptr);
      dapSrcIdx [dapCount].swap (tmp);
      localIdx[inputChannel.inputId] = dapCount;
      dapChannelIdx.push_back (inputChannel.getRelIdx ());
      dapDoWeight.push_back (inputChannel.dapWeight);
      // XXX YYY feature a faire plus tard (au moment du calcul de AP)
      dapSrcIdx [dapCount][0] = getSrcOrig4Dap (inputChannel); // XXX getPixelsDapProducer
      for (DimChannel i = 0; i < apSrc.size (); ++i)
	dapSrcIdx [dapCount][i+1] = apSrc [i].data ();
      break;
    }

    case Haar:
    case Stat:
      break;
    }
  }
}

// ================================================================================
template <typename FeatureT>
void
FeatureOutput<FeatureT>::setMatrixRowData (RealMatrix &inputs, DimChannel &channel, const size_t &row, const DimImg &pixelId) const {
  for (DimChannel i = 0; i < copySrcData.size (); ++channel, ++i)
    inputs (row, channel) = copySrcData [i][pixelId];

  for (DimImg dapId = 0; dapId < dapChannelIdx.size (); ++dapId) {
    const vector<DimChannel> &channelIdx (dapChannelIdx [dapId]);
    const vector<FeatureT*> &srcIdx (dapSrcIdx [dapId]);
    const bool doWeight (dapDoWeight [dapId]);
    const DimChannel dapOutputSize (channelIdx.size ()/2);

    for (DimImg localChannel = 0; localChannel < dapOutputSize; ++localChannel, ++channel) {
      // XXX YYY feature
      double dapValue = srcIdx [channelIdx [localChannel*2]][pixelId] - srcIdx [channelIdx [localChannel*2+1]][pixelId];
      if (doWeight && srcIdx [0])
	dapValue *= double (srcIdx [0][pixelId]);
      if (dapValue < 0)
	dapValue = - dapValue;
      inputs (row, channel) = dapValue;
    }
  }
}

// ================================================================================
template <typename FeatureT>
template <typename PixelT>
void
FeatureOutput<FeatureT>::writeFeatures (Broceliande<PixelT> &broceliande, DimChannel &channel) {
  DEF_LOG ("FeatureOutput<FeatureT>::writeFeatures", "channel" << channel);
  const OptChannel &optChannel (broceliande.optChannel);
  const DimImg pixelCount (broceliande.getPixelCount ());
  DimChannel channelCount (0);
  // DimChannel channelMax (0); // XXX vérif
  for (const InputChannel *inputChannelP : optChannel.getActiveOrder ()) {
    const InputChannel &inputChannel (*inputChannelP);
    if (broceliande.getFeaturesOutput ().channelsFeaturesOutputType [inputChannel.inputId] != featureOutputType ||
	!inputChannel.doWrite)
      continue;
    switch (inputChannel.channelType) {

    case Original:
      broceliande.writeBand (orig4Copy [localIdx[inputChannel.inputId]].data (), inputChannel.outputRank);
      ++channelCount;
      break;

    case Ndvi:
      broceliande.writeBand (ndviRasters [localIdx[inputChannel.inputId]].getPixelsVector ().data (), inputChannel.outputRank);
      ++channelCount;
      break;

    case Pantex:
      broceliande.writeBand (pantexRasters [localIdx[inputChannel.inputId]].getPixelsVector ().data (), inputChannel.outputRank);
      ++channelCount;
      break;

    case Sobel:
      broceliande.writeBand (sobelRasters [localIdx[inputChannel.inputId]].getPixelsVector ().data (), inputChannel.outputRank);
      ++channelCount;
      break;

    case AP:{
      const DimImg apId = localIdx[inputChannel.inputId];
      vector<vector<FeatureT> > &apSrc (apOutput[apId]);
      for (DimChannel localChannel = 0; localChannel < apSrc.size (); ++localChannel)
      	broceliande.writeBand (apSrc [localChannel].data (), inputChannel.outputRank+localChannel);
      channelCount += apSrc.size ();
      break;
    }

    case DAP: {
      const DimImg dapId = localIdx[inputChannel.inputId];
      const vector<DimChannel> &channelIdx (dapChannelIdx [dapId]);
      const vector<FeatureT*> &srcIdx (dapSrcIdx [dapId]);
      const bool doWeight (dapDoWeight [dapId]);
      const DimChannel dapOutputSize (channelIdx.size ()/2);

      vector<vector<double> > dapOutput (dapOutputSize, vector<double> (pixelCount));
      for (DimImg pixelId = 0; pixelId < pixelCount; ++pixelId)
	for (DimImg localChannel = 0; localChannel < dapOutputSize; ++localChannel) {
	  // XXX YYY feature
	  double dapValue = srcIdx [channelIdx [localChannel*2]][pixelId] - srcIdx [channelIdx [localChannel*2+1]][pixelId];
	  if (doWeight && srcIdx [0])
	    dapValue *= srcIdx [0][pixelId];
	  if (dapValue < 0)
	    dapValue = - dapValue;
	  dapOutput [localChannel][pixelId] = dapValue;
	}
      for (DimChannel localChannel = 0; localChannel < dapOutput.size (); ++localChannel)
	broceliande.writeBand (dapOutput[localChannel].data (), channel+localChannel);
      channelCount += dapOutputSize;
      break;
    }
    case Haar:
    case Stat:
      break;
    }
  }
  channel += channelCount;
}

// ================================================================================
template <typename FeatureT>
FeatureT*
FeatureOutput<FeatureT>::getSrcOrig4Dap (const InputChannel &inputChannel) {
  DEF_LOG ("FeatureOutput::getSrcOrig4Dap", "");
  BOOST_ASSERT (inputChannel.channelType == DAP);
  const InputChannel &srcChannel (* inputChannel.generateBy [0]->generateBy[0]);
  switch (srcChannel.channelType) {

    // XXX sauf si feature
  case Original:
    if (srcChannel.doWrite) {
      LOG ("inputCopy:" << inputChannel.inputId);
      BOOST_ASSERT (localIdx[srcChannel.inputId] < orig4Copy.size ());
      return orig4Copy [localIdx[srcChannel.inputId]].data ();
    }
    LOG ("inputDAP:" << inputChannel.inputId);
    BOOST_ASSERT (localIdx[srcChannel.inputId] < orig4Dap.size ());
    return orig4Dap [localIdx[srcChannel.inputId]].data ();

  case Ndvi:
    LOG ("ndviRasters:" << inputChannel.inputId);
    BOOST_ASSERT (localIdx[srcChannel.inputId] < ndviRasters.size ());
    return ndviRasters [localIdx[srcChannel.inputId]].getPixelsVector ().data ();

  case Pantex:
    LOG ("pantexRaster:" << inputChannel.inputId);
    BOOST_ASSERT (localIdx[srcChannel.inputId] < pantexRasters.size ());
    return pantexRasters [localIdx[srcChannel.inputId]].getPixelsVector ().data ();

  case Sobel:
    LOG ("sobelRaster:" << inputChannel.inputId);
    BOOST_ASSERT (localIdx[srcChannel.inputId] < sobelRasters.size ());
    return sobelRasters [localIdx[srcChannel.inputId]].getPixelsVector ().data ();

  default:
    BOOST_ASSERT (false);
    cerr << "Broceliande::getPixelsDapProducer: input channel not found for " << srcChannel << endl;
    exit(1);
  }
  // XXX if not PixelT
  // return null;
}

// ================================================================================
template <typename PixelT>
FeaturesOutput<PixelT>::FeaturesOutput (const Broceliande<PixelT> &broceliande) :
  broceliande (broceliande),
  channelsFeaturesOutputType (broceliande.optChannel.getMaxInputIdCount (), PixelFeature) {

  for (const InputChannel *inputChannelP : broceliande.optChannel.getActiveOrder ()) {
    const InputChannel &inputChannel (*inputChannelP);
    switch (inputChannel.featureType) {
    case FArea:
    case FPerimeter:
      channelsFeaturesOutputType [inputChannel.inputId] = DimFeature;
      break;

    case FZLength:
      channelsFeaturesOutputType [inputChannel.inputId] = ChannelFeature;
      break;

    case FSTS:
    case FCompactness:
    case FComplexity:
    case FSimplicity:
    case FRectangularity:
    case FSD:
    case FSDW:
    // case FSDA:
    case FMoI:
      channelsFeaturesOutputType [inputChannel.inputId] = DoubleFeature;
      break;

    case FMin:
    case FMax:
    case FMean:
    case FAP:
      channelsFeaturesOutputType [inputChannel.inputId] = PixelFeature;
    }
  }
  pixelFeature.init (*this, PixelFeature);
  dimFeature.init (*this, DimFeature);
  channelFeature.init (*this, ChannelFeature);
  doubleFeature.init (*this, DoubleFeature);
}

template <typename PixelT>
FeaturesOutput<PixelT>::~FeaturesOutput () {
}

// ================================================================================
template <typename PixelT>
void
FeaturesOutput<PixelT>::setMatrixRowData (RealMatrix &inputs, DimChannel &channel, const size_t &row, const DimImg &pixelId) const {
  pixelFeature.setMatrixRowData (inputs, channel, row, pixelId);
  dimFeature.setMatrixRowData (inputs, channel, row, pixelId);
  channelFeature.setMatrixRowData (inputs, channel, row, pixelId);
  doubleFeature.setMatrixRowData (inputs, channel, row, pixelId);
}

template <typename PixelT>
void
FeaturesOutput<PixelT>::writeFeatures (Broceliande<PixelT> &broceliande, DimChannel &channel) {
  pixelFeature.writeFeatures (broceliande, channel);
  dimFeature.writeFeatures (broceliande, channel);
  channelFeature.writeFeatures (broceliande, channel);
  doubleFeature.writeFeatures (broceliande, channel);
}


// ================================================================================
