////////////////////////////////////////////////////////////////////////////
// Copyright IRISA 2017							  //
// 									  //
// triskele.obelix (at) irisa.fr					  //
// 									  //
// This  software is  a computer  program  whose purpose  is to  classify //
// pixels by using TRISKELE and random forest.				  //
// 									  //
// This software is governed by the CeCILL-B license under French law and //
// abiding by  the rules of distribution  of free software. You  can use, //
// modify  and/or  redistribute  the  software under  the  terms  of  the //
// CeCILL-B license as circulated by CEA, CNRS and INRIA at the following //
// URL "http://www.cecill.info".					  //
// 									  //
// As a counterpart to the access to  the source code and rights to copy, //
// modify and  redistribute granted  by the  license, users  are provided //
// only with a limited warranty and  the software's author, the holder of //
// the economic  rights, and the  successive licensors have  only limited //
// liability.								  //
// 									  //
// In this respect, the user's attention is drawn to the risks associated //
// with loading,  using, modifying  and/or developing or  reproducing the //
// software by the user in light of its specific status of free software, //
// that may  mean that  it is  complicated to  manipulate, and  that also //
// therefore means  that it  is reserved  for developers  and experienced //
// professionals having in-depth computer  knowledge. Users are therefore //
// encouraged  to load  and test  the software's  suitability as  regards //
// their  requirements  in  conditions  enabling the  security  of  their //
// systems and/or  data to  be ensured  and, more  generally, to  use and //
// operate it in the same conditions as regards security.		  //
// 									  //
// The fact that  you are presently reading this means  that you have had //
// knowledge of the CeCILL-B license and that you accept its terms.	  //
////////////////////////////////////////////////////////////////////////////

#include <algorithm>

#include "vectorMisc.hpp"
//#include <algorithm>    // std::transform

using namespace obelix;

// ================================================================================
// Supprime les éléments d'un vecteur, en fonction de ses index ou valeurs
void
obelix::removeInVector (vector<DimImg> &toClean, const vector<DimImg> &sortedBadValues, const bool &indexAndNotValue) {
  int vectorCount = toClean.size ();
  // On vérifie si on utilise les index ou les valeurs pour filtrer
  if (indexAndNotValue) {
    for (typename vector<DimImg>::const_reverse_iterator rit = sortedBadValues.crbegin (); rit != sortedBadValues.crend (); ++rit)
      std::swap (toClean[*rit], toClean[--vectorCount]);
  } else {
    typename vector<DimImg>::iterator begin = toClean.begin (), lastUnknown = toClean.end ();
    for (typename vector<DimImg>::const_reverse_iterator rit = sortedBadValues.crbegin (); rit != sortedBadValues.crend (); ++rit) {
      lastUnknown = lower_bound (begin, lastUnknown, *rit);
      std::swap (toClean[lastUnknown - begin], toClean[--vectorCount]);
    }
  }
  toClean.resize (vectorCount);
  sort (toClean.begin (), toClean.end ());
}

// ================================================================================
// converti les index d'un vecteur vers un autre en valeurs pointés dans l'autre
void
obelix::toRealVector (vector<DimImg> &realVector, const vector<DimImg> &indexVector, const vector<DimImg> &refVector) {
  realVector.reserve (indexVector.size ());
  // XXX
  // transform (indexVector.cbegin (), indexVector.cend (), realVector.begin (), [&refVector] (const DimImg &v) { refVector[v]; });
  for (typename vector<DimImg>::const_iterator it = indexVector.cbegin (); it != indexVector.cend (); ++it)
    realVector.push_back (refVector[*it]);
}

// ================================================================================
// Supprime les éléments d'un vecteur, en fonction d'un vector d'index ou de valeurs
void
obelix::removeInIndirectVector (vector<DimImg> &toClean, const vector<DimImg> &sortedBadIndex) {
  if (! sortedBadIndex.size ())
    return;
  DimImg shift = 0;
  for (DimImg idBad = 0, from = sortedBadIndex[idBad]; idBad < sortedBadIndex.size (); ++idBad) {
    DimImg to = (idBad == sortedBadIndex.size () - 1) ? toClean.size () : sortedBadIndex[idBad + 1];
    ++shift, ++from;
    for (; from < to ; ++from)
      toClean[from - shift] = toClean[from] - shift;
  }
  toClean.resize (toClean.size () - shift);
}

// ================================================================================
