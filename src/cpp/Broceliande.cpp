////////////////////////////////////////////////////////////////////////////
// Copyright IRISA 2017							  //
// 									  //
// triskele.obelix (at) irisa.fr					  //
// 									  //
// This  software is  a computer  program  whose purpose  is to  classify //
// pixels by using TRISKELE and random forest.				  //
// 									  //
// This software is governed by the CeCILL-B license under French law and //
// abiding by  the rules of distribution  of free software. You  can use, //
// modify  and/or  redistribute  the  software under  the  terms  of  the //
// CeCILL-B license as circulated by CEA, CNRS and INRIA at the following //
// URL "http://www.cecill.info".					  //
// 									  //
// As a counterpart to the access to  the source code and rights to copy, //
// modify and  redistribute granted  by the  license, users  are provided //
// only with a limited warranty and  the software's author, the holder of //
// the economic  rights, and the  successive licensors have  only limited //
// liability.								  //
// 									  //
// In this respect, the user's attention is drawn to the risks associated //
// with loading,  using, modifying  and/or developing or  reproducing the //
// software by the user in light of its specific status of free software, //
// that may  mean that  it is  complicated to  manipulate, and  that also //
// therefore means  that it  is reserved  for developers  and experienced //
// professionals having in-depth computer  knowledge. Users are therefore //
// encouraged  to load  and test  the software's  suitability as  regards //
// their  requirements  in  conditions  enabling the  security  of  their //
// systems and/or  data to  be ensured  and, more  generally, to  use and //
// operate it in the same conditions as regards security.		  //
// 									  //
// The fact that  you are presently reading this means  that you have had //
// knowledge of the CeCILL-B license and that you accept its terms.	  //
////////////////////////////////////////////////////////////////////////////

#include <iostream>
#include <algorithm>
#include <boost/chrono.hpp>
#include <boost/filesystem.hpp>
#include <shark/Data/Dataset.h>
#include <shark/Models/Trees/RFClassifier.h>
#include <shark/Algorithms/Trainers/RFTrainer.h>

#include "obelixNdvi.hpp"
#include "obelixPantex.hpp"
#include "obelixSobel.hpp"
#include "TreeStats.hpp"
#include "ArrayTree/triskeleArrayTreeBase.hpp"
#include "ArrayTree/ArrayTreeBuilder.hpp"
#include "Attributes/WeightAttributes.hpp"
#include "AttributeProfiles.hpp"
#include "Attributes/AttributesCache.hpp"

#include "Broceliande.hpp"

using namespace shark;
using namespace obelix::broceliande;
#include "Broceliande.tcpp"

using namespace boost::chrono;

using namespace obelix::triskele;

// ================================================================================
template <typename PixelT>
Raster<PixelT, 2> &
Broceliande<PixelT>::getProducerRaster (const InputChannel &inputChannel) {
  DEF_LOG ("Broceliande::getProducerRaster", "");
  switch (inputChannel.channelType) {
  case Original:
    LOG ("inputRaster");
    return inputRaster;
  case Ndvi:
    LOG ("ndviRaster:" << inputChannel.inputId);
    return featuresOutput.pixelFeature.getNdvi (inputChannel.inputId);
  case Pantex:
    LOG ("pantexRaster:" << inputChannel.inputId);
    return featuresOutput.pixelFeature.getPantex (inputChannel.inputId);
  case Sobel:
    LOG ("sobelRaster:" << inputChannel.inputId);
    return featuresOutput.pixelFeature.getSobel (inputChannel.inputId);
  default:
    throw invalid_argument ("getProducerRaster not input channel");
  }
}

// ================================================================================
template <typename PixelT>
Broceliande<PixelT>::Broceliande (IImage<2> &inputImage, IImage<2> &outputImage,
				  const Point<2> &topLeft, const Size<2> &size, OptBroceliande &optBroceliande)
  : optChannel (optBroceliande.optChannel),
    inputImage (inputImage),
    outputImage (outputImage),
    noWriteFlag (outputImage.getFileName ().empty ()),
    optBroceliande (optBroceliande),

    topLeft (topLeft),
    size (size),
    border (size, !optBroceliande.includeNoDataFlag),
    graphWalker (border, optBroceliande.connectivity, optBroceliande.countingSortCeil),
    pixelCount (size.getPixelsCount ()),
    groundTruth (optBroceliande.coreCount),
    allBandsCount (optBroceliande.optChannel.getOutputCount ()),

    inputRaster (size),
    outputPixRaster (optBroceliande.classificationFlag ? NullSize2D : size),

    integralImages (optChannel.getIntegralImageCount (), vector<double> (pixelCount)),
    doubleIntegralImages (optChannel.getDoubleIntegralImageCount (), vector<double> (pixelCount)),
    haars (optChannel.getHaarCount ()),
    stats (optChannel.getStatCount ()),

    featuresOutput (*this),

    iiHaarIdx (optChannel.getHaarCount ()),
    iiStatIdx (optChannel.getStatCount ()),
    diiStatIdx (optChannel.getStatCount ()),

    tree (optBroceliande.coreCount),
    weightAttributes (tree, false) {

  DEF_LOG ("Broceliande::Broceliande", "size: " << size << " pixelCount: " << pixelCount);
  // maxD = (1UL << 8) - 1;
  // halfMaxD = maxD/2;

  for (const pair<DimChannel, const InputChannel *> &item : optChannel.getHaarOrder ()) {
    const InputChannel &inputChannel (*item.second);
    iiHaarIdx [inputChannel.typeRank] = inputChannel.integralImageRank;
    for (const Size<2> pattern : inputChannel.winSizes)
      haars[inputChannel.typeRank].push_back (TextWin<2> (size, pattern));
  }
  for (const pair<DimChannel, const InputChannel *> &item : optChannel.getStatOrder ()) {
    const InputChannel &inputChannel (*item.second);
    iiStatIdx [inputChannel.typeRank] = inputChannel.integralImageRank;
    diiStatIdx [inputChannel.typeRank] = inputChannel.doubleIntegralImageRank;
    for (const Size<2> pattern : inputChannel.winSizes)
      stats[inputChannel.typeRank].push_back (TextWin<2> (size, pattern));
  }

}

template <typename PixelT>
Broceliande<PixelT>::~Broceliande () {
}

// ================================================================================
template <typename PixelT>
void
Broceliande<PixelT>::parseInput () {
  DEF_LOG ("Broceliande::parseInput", "");
  // manage border
  LOG ("read border");
  border.reset (!optBroceliande.includeNoDataFlag);
  if (!optBroceliande.includeNoDataFlag) {
    for (const InputChannel *inputChannelP : optChannel.getOriginal ()) {
      if (!inputChannelP->prodWrite)
	continue;
      const double doubleValue (isnan (optBroceliande.noDataValue) ?
				inputImage.getNoDataValue (inputChannelP->inputId) :
				optBroceliande.noDataValue);
      if (isnan (doubleValue))
	continue;
      const PixelT noDataValue (doubleValue);
      auto start = high_resolution_clock::now ();
      inputImage.readBand<PixelT> (inputRaster, inputChannelP->inputId, topLeft, size);
      TreeStats::global.addTime (readStats, duration_cast<duration<double> > (high_resolution_clock::now ()-start).count ());

      start = high_resolution_clock::now ();
      PixelT *inputPixels = inputRaster.getPixels ();
      for (DimImg pixelId = 0; pixelId < pixelCount; ++pixelId)
	if (inputPixels [pixelId] != noDataValue)
	  border.clearBorder (pixelId);
      TreeStats::global.addTime (borderStats, duration_cast<duration<double> > (high_resolution_clock::now ()-start).count ());
    }
    border.reduce ();
  }
  
  vector<InputChannel *> activeOrderedChannels = optChannel.getActiveOrder ();
  map<const InputChannel *, map<TreeType, map<AttributeType, const InputChannel *> > > waitingAP;
  vector<vector<double> > gTmp;

  // direct production
  for (InputChannel *inputChannelP : activeOrderedChannels) {
    const InputChannel &inputChannel (*inputChannelP);
    if (inputChannel.channelType == Original) {
      LOG ("read input");
      auto start = high_resolution_clock::now ();
      inputImage.readBand<PixelT> (inputRaster, inputChannel.inputId, topLeft, size);
      TreeStats::global.addTime (readStats, duration_cast<duration<double> > (high_resolution_clock::now ()-start).count ());

      if (inputChannel.doWrite) {
	LOG ("copy input");
	saveOrig4Copy (inputRaster, inputChannel.inputId);
      } else if (inputChannel.prodWrite && inputChannel.prodDap) {
	LOG ("origDAP input");
	saveOrig4Dap (inputRaster, inputChannel.inputId);
      }
    }
      
    for (InputChannel *childP : inputChannel.produce) {
      if (!childP->prodWrite)
	continue;
      switch (childP->channelType) {

      case Ndvi: {
	LOG ("produce new bands");
	auto start = high_resolution_clock::now ();
	computeNdvi (getProducerRaster (inputChannel),
		     featuresOutput.pixelFeature.getNdvi (childP->inputId),
		     childP->generateBy [0] == &inputChannel,
		     childP->generateBy [0]->inputId > childP->generateBy [1]->inputId);
	TreeStats::global.addTime (ndviStats, duration_cast<duration<double> > (high_resolution_clock::now ()-start).count ());
	break;
      }

      case Pantex: {
	LOG ("produce new bands");
	auto start = high_resolution_clock::now ();
	computePantex (getProducerRaster (inputChannel), featuresOutput.pixelFeature.getPantex (childP->inputId), optBroceliande.coreCount, optBroceliande.pantexWindowSide);
	TreeStats::global.addTime (pantexStats, duration_cast<duration<double> > (high_resolution_clock::now ()-start).count ());
	break;
      }

      case Sobel: {
	LOG ("produce new bands");
	auto start = high_resolution_clock::now ();
	computeSobel (getProducerRaster (inputChannel), featuresOutput.pixelFeature.getSobel (childP->inputId), gTmp, optBroceliande.coreCount);
	TreeStats::global.addTime (sobelStats, duration_cast<duration<double> > (high_resolution_clock::now ()-start).count ());
	break;
      }

      default:
	break;
      }
    }
    if (inputChannel.prodInetgralImage) {
      LOG ("produce arithmetic bands");
      auto start = high_resolution_clock::now ();
      computeIntegralImage (size, getProducerRaster (inputChannel).getPixels (), integralImages [inputChannel.integralImageRank].data ());
      TreeStats::global.addTime (iiStats, duration_cast<duration<double> > (high_resolution_clock::now ()-start).count ());
    }
    if (inputChannel.prodDoubleInetgralImage) {
      auto start = high_resolution_clock::now ();
      DimChannel iiRank = inputChannel.doubleIntegralImageRank;
      doubleMatrix (size, getProducerRaster (inputChannel).getPixels (), doubleIntegralImages [iiRank].data ());
      computeIntegralImage (size, doubleIntegralImages[iiRank].data (), doubleIntegralImages [iiRank].data ());
      TreeStats::global.addTime (iiStats, duration_cast<duration<double> > (high_resolution_clock::now ()-start).count ());
    }
  }

  // AP production
  LOG ("tree production");
  for (InputChannel *inputChannelP : activeOrderedChannels)
    if (inputChannelP->channelType == AP)
      waitingAP [inputChannelP->generateBy[0]] [inputChannelP->treeType] [inputChannelP->attributeType] = inputChannelP;

  for (InputChannel *inputChannelP : activeOrderedChannels) {
    InputChannel &inputChannel (*inputChannelP);
    if (waitingAP[inputChannelP].empty ())
      continue;
    if (inputChannel.channelType == Original)
      inputImage.readBand<PixelT> (inputRaster, inputChannel.inputId, topLeft, size);
    Raster<PixelT, 2> &tmpRaster (getProducerRaster (inputChannel));
    for (const pair<TreeType, map <AttributeType, const InputChannel*> > &item : waitingAP[inputChannelP]) {
      const TreeType &treeType = item.first;
      LOG ("treeType: " << treeType);
      ArrayTreeBuilder<PixelT, PixelT, 2> atb (tmpRaster, graphWalker, treeType, optBroceliande.threadPerImage, optBroceliande.tilePerThread, optBroceliande.seqTileFlag, optBroceliande.autoThreadFlag, false);
      weightAttributes.resetDecr (getDecrFromTreetype (inputChannel.treeType));
      atb.buildTree (tree, weightAttributes, optBroceliande.timeFlag);
      AttributesCache<PixelT, 2> attributesCache (tree, graphWalker, tmpRaster, weightAttributes);

      switch (inputChannel.featureType) {
      case FAP:
	doFeature<PixelT> (featuresOutput.pixelFeature, atb, attributesCache,
			   [] (AttributeProfiles<PixelT, 2> &attributeProfiles, ArrayTreeBuilder<PixelT, PixelT, 2> &atb) {
			     atb.setAttributProfiles (attributeProfiles);
			   }, item.second);
	break;
      case FArea:
	doFeature<DimImgPack> (featuresOutput.dimFeature, atb, attributesCache,
			       [&attributesCache] (AttributeProfiles<DimImgPack, 2> &attributeProfiles, ArrayTreeBuilder<PixelT, PixelT, 2> &atb) {
				 attributeProfiles.setValues (1, attributesCache.getArea ().getValues ());
			       }, item.second);
	break;
      case FPerimeter:
	doFeature<DimImgPack> (featuresOutput.dimFeature, atb, attributesCache,
			       [&attributesCache] (AttributeProfiles<DimImgPack, 2> &attributeProfiles, ArrayTreeBuilder<PixelT, PixelT, 2> &atb) {
				 attributeProfiles.setValues (1., attributesCache.getPerimeter ().getValues ());
			       }, item.second);
	break;
	// case FZLength:
	//   doFeature<DimChannel> (featuresOutput.channelFeature, atb, attributesCache,
	// 			 [&attributesCache] (AttributeProfiles<DimChannel, 2> &attributeProfiles, ArrayTreeBuilder<PixelT, PixelT, 2> &atb) {
	// 			   attributeProfiles.setValues (1., attributesCache.getZLength ().getValues ());
	// 			 }, item.second);
	//   break;
	// case FSTS:
	//   doFeature<double> (featuresOutput.channelFeature, atb, attributesCache,
	// 		     [&attributesCache] (AttributeProfiles<double, 2> &attributeProfiles, ArrayTreeBuilder<PixelT, PixelT, GeoDimT> &atb) {
	// 		       attributeProfiles.setValues (0., attributesCache.getSTS ().getValues ());
	// 		     }, item.second);
	//   break;
      case FCompactness:
	doFeature<double> (featuresOutput.doubleFeature, atb, attributesCache,
			   [&attributesCache] (AttributeProfiles<double, 2> &attributeProfiles, ArrayTreeBuilder<PixelT, PixelT, 2> &atb) {
			     attributeProfiles.setValues (1., attributesCache.getCompactness ().getValues ());
			   }, item.second);
	break;
      case FComplexity:
	doFeature<double> (featuresOutput.doubleFeature, atb, attributesCache,
			   [&attributesCache] (AttributeProfiles<double, 2> &attributeProfiles, ArrayTreeBuilder<PixelT, PixelT, 2> &atb) {
			     attributeProfiles.setValues (1., attributesCache.getComplexity ().getValues ());
			   }, item.second);
	break;
      case FSimplicity:
	doFeature<double> (featuresOutput.doubleFeature, atb, attributesCache,
			   [&attributesCache] (AttributeProfiles<double, 2> &attributeProfiles, ArrayTreeBuilder<PixelT, PixelT, 2> &atb) {
			     attributeProfiles.setValues (1., attributesCache.getSimplicity ().getValues ());
			   }, item.second);
	break;
      case FRectangularity:
	doFeature<double> (featuresOutput.doubleFeature, atb, attributesCache,
			   [&attributesCache] (AttributeProfiles<double, 2> &attributeProfiles, ArrayTreeBuilder<PixelT, PixelT, 2> &atb) {
			     attributeProfiles.setValues (1., attributesCache.getRectangularity ().getValues ());
			   }, item.second);
	break;

      case FMin:
	doFeature<PixelT> (featuresOutput.pixelFeature, atb, attributesCache,
			   [&attributesCache] (AttributeProfiles<PixelT, 2> &attributeProfiles, ArrayTreeBuilder<PixelT, PixelT, 2> &atb) {
			     attributeProfiles.setValues (attributesCache.getRaster ().getPixels (), attributesCache.getMin ().getValues ());
			   }, item.second);
	break;
      case FMax:
	doFeature<PixelT> (featuresOutput.pixelFeature, atb, attributesCache,
			   [&attributesCache] (AttributeProfiles<PixelT, 2> &attributeProfiles, ArrayTreeBuilder<PixelT, PixelT, 2> &atb) {
			     attributeProfiles.setValues (attributesCache.getRaster ().getPixels (), attributesCache.getMax ().getValues ());
			   }, item.second);
	break;
      case FMean:
	doFeature<PixelT> (featuresOutput.pixelFeature, atb, attributesCache,
			   [&attributesCache] (AttributeProfiles<PixelT, 2> &attributeProfiles, ArrayTreeBuilder<PixelT, PixelT, 2> &atb) {
			     attributeProfiles.setValues (attributesCache.getRaster ().getPixels (), attributesCache.getMean ().getValues ());
			   }, item.second);
	break;

      case FSD:
	doFeature<double> (featuresOutput.doubleFeature, atb, attributesCache,
			   [&attributesCache] (AttributeProfiles<double, 2> &attributeProfiles, ArrayTreeBuilder<PixelT, PixelT, 2> &atb) {
			     attributeProfiles.setValues (0., attributesCache.getSD ().getValues ());
			   }, item.second);
	break;

      case FSDW:
	doFeature<double> (featuresOutput.doubleFeature, atb, attributesCache,
			   [&attributesCache] (AttributeProfiles<double, 2> &attributeProfiles, ArrayTreeBuilder<PixelT, PixelT, 2> &atb) {
			     attributeProfiles.setValues (0., attributesCache.getSDW ().getValues ());
			   }, item.second);
	break;
	// case FSDA:
	//   doFeature<double> (featuresOutput.doubleFeature, atb, attributesCache,
	// 		     [&attributesCache] (AttributeProfiles<double, 2> &attributeProfiles, ArrayTreeBuilder<PixelT, PixelT, 2> &atb) {
	// 		       attributeProfiles.setValues (0., attributesCache.getSDA ().getValues ());
	// 		     }, item.second);
	//   break;
      case FMoI:
	doFeature<double> (featuresOutput.doubleFeature, atb, attributesCache,
			   [&attributesCache] (AttributeProfiles<double, 2> &attributeProfiles, ArrayTreeBuilder<PixelT, PixelT, 2> &atb) {
			     attributeProfiles.setValues (0., attributesCache.getMoI ().getValues ());
			   }, item.second);
	break;
      default:
	cerr << "*** Warning: Feature Profile (" << inputChannel.featureType << ") no implemented!" << endl;
	BOOST_ASSERT (false);
      }
    }
  }
}

// ================================================================================
template <typename PixelT>
template <typename FeatureT, typename APFunct>
void
Broceliande<PixelT>::doFeature (FeatureOutput<FeatureT> &featureOutput, ArrayTreeBuilder<PixelT, PixelT, 2> &atb, AttributesCache<PixelT, 2> &attributesCache,
				const APFunct &setAP, const map <AttributeType, const InputChannel*> &attrProd) {
  DEF_LOG ("Broceliande::doFeature", "");
  AttributeProfiles<FeatureT, 2> attributeProfiles (tree);
  setAP (attributeProfiles, atb);

  for (const pair<AttributeType, const InputChannel*> &item2 : attrProd) {
    const InputChannel &srcChannel (* item2.second);
    LOG ("apChannel: " << srcChannel);
    const AttributeType &attributeType (srcChannel.attributeType);
    LOG ("attributeType: " << attributeType);
    vector<vector<FeatureT> > &apOutput (featureOutput.getAp (srcChannel.inputId));

    switch (attributeType) {
    case AArea: {
      vector<DimImgPack> thresholds (attributesCache.getArea ().getConvertedThresholds (srcChannel.thresholds));
      attributesCache.getArea ().cut (apOutput, attributeProfiles, thresholds);
    } break;
    case APerimeter: {
      vector<DimImgPack> thresholds (attributesCache.getPerimeter ().getConvertedThresholds (srcChannel.thresholds));
      attributesCache.getPerimeter ().cut (apOutput, attributeProfiles, thresholds, Max);
    } break;
    // case AZLength: {
    //   vector<DimChannel> thresholds (attributesCache.getZLength ().getConvertedThresholds (srcChannel.thresholds));
    //   attributesCache.getZLength ().cut (apOutput, attributeProfiles, thresholds);
    // } break;
    // case ASTS: {
    //   attributesCache.getSTS ().cut (apOutput, attributeProfiles, srcChannel.thresholds, Max);
    // } break;
    case ACompactness: {
      attributesCache.getCompactness ().cut (apOutput, attributeProfiles, srcChannel.thresholds, Max);
    } break;
    case AComplexity: {
      attributesCache.getComplexity ().cut (apOutput, attributeProfiles, srcChannel.thresholds, Max);
    } break;
    case ASimplicity: {
      attributesCache.getSimplicity ().cut (apOutput, attributeProfiles, srcChannel.thresholds, Max);
    } break;
    case ARectangularity: {
      attributesCache.getRectangularity ().cut (apOutput, attributeProfiles, srcChannel.thresholds, Max);
    } break;
    case AWeight: {
      vector<PixelT> thresholds (attributesCache.getWeight ().getConvertedThresholds (srcChannel.thresholds));
      attributesCache.getWeight ().cut (apOutput, attributeProfiles, thresholds);
    } break;
    case ASD: {
      attributesCache.getSD ().cut (apOutput, attributeProfiles, srcChannel.thresholds);
    } break;
    case ASDW: {
      attributesCache.getSDW ().cut (apOutput, attributeProfiles, srcChannel.thresholds);
    } break;
      // case ASDA: {
      //   attributesCache.getSDA ().cut (apOutput, attributeProfiles, srcChannel.thresholds);
      // } break;
    case AMoI: {
      attributesCache.getMoI ().cut (apOutput, attributeProfiles, srcChannel.thresholds);
    } break;
    default:
      cerr << "*** Warning: cut (" << attributeType << ") no implemented!" << endl;
      BOOST_ASSERT (false);
    }
  }
}


// ================================================================================
template <typename PixelT>
void
Broceliande<PixelT>::snapshot () {
  DEF_LOG ("Broceliande::snapshot", "");
  if (noWriteFlag || optBroceliande.classificationFlag)
    return;

  outputImage.createImage (size, optChannel.getOutputImageType (getGDALType (PixelT (0))), optChannel.getOutputCount (), inputImage, topLeft);

  DimChannel channel (0);

  featuresOutput.writeFeatures (*this, channel);

  // XXX Haar and Stat only 2D (C8 connectivity)
  vector<vector<double> > haarOutput (4, vector<double> (pixelCount));
  DimChannel haarId (0);
  for (const vector<TextWin<2> > &textWins : haars) {
    const double *integralImageP (integralImages [iiHaarIdx [haarId]].data ());
    DimChannel textWinId (0);
    for (const TextWin<2> &textWin : textWins) {
      for (DimImg pixelId = 0; pixelId < pixelCount; ++pixelId) {
	// XXX Haar and Stat only 2D (C8 connectivity)
	Point<2> p = idx2point (size, pixelId);
	double hx (textWin.getHx (integralImageP, p.coord[0], p.coord[1], pixelId));
	double hy (textWin.getHy (integralImageP, p.coord[0], p.coord[1], pixelId));
	haarOutput [0][pixelId] = hx;		// hx+M/2 => pixel
	haarOutput [1][pixelId] = hx+hy;	// (hx+hy)/2+M/2 => pixel
	haarOutput [2][pixelId] = hy;		// hx+M/2 => pixel
	haarOutput [3][pixelId] = hy-hx;	// (hx+hy)/2+M/2 => pixel
	++textWinId;
      }
      for (DimChannel localChannel = 0; localChannel < 4; ++localChannel, ++channel)
	writeBand (haarOutput[localChannel].data (), channel);
      ++haarId;
    }
  }
  haarOutput = vector<vector<double> > ();

  vector<vector<double> > statOutput (3, vector<double> (pixelCount));
  DimChannel statId (0);
  double mean, std, ent;
  for (const vector<TextWin<2> > &textWins : stats) {
    const double *integralImageP (integralImages [iiStatIdx [statId]].data ());
    const double *doubleIntegralImageP (doubleIntegralImages [diiStatIdx [statId]].data ());
    DimChannel textWinId (0);
    for (const TextWin<2> &textWin : textWins) {
      // XXX Haar and Stat only 2D (C8 connectivity)
      for (DimImg pixelId = 0; pixelId < pixelCount; ++pixelId) {
	Point<2> p = idx2point (size, pixelId);
	textWin.getMeanStdEnt (mean, std, ent, integralImageP, doubleIntegralImageP, p.coord[0], p.coord[1], pixelId);
	statOutput [0][pixelId] = mean;	// => pixel
	statOutput [1][pixelId] = std;	// => double
	statOutput [2][pixelId] = ent;	// => double
	++textWinId;
      }
      for (DimChannel localChannel = 0; localChannel < 3; ++localChannel, ++channel)
	writeBand (statOutput[localChannel].data (), channel);
      ++statId;
    }
  }
  statOutput = vector<vector<double> > ();
  outputImage.close ();
}

// ================================================================================
template <typename PixelT>
void
Broceliande<PixelT>::training (RFClassifier<SharkLabelType> &classifier) {
  DEF_LOG ("Broceliande::training", "");

  auto start = high_resolution_clock::now ();
  RFTrainer<SharkLabelType> trainer (false, false);
  LabeledData<RealVector, SharkLabelType> trainingData;
  trainer.setNTrees (optBroceliande.optTraining.nTrees);
  trainer.setMTry (sqrt (optChannel.getOutputCount ()));
  getSamples (trainingData, GroundTruth::sampleSet);
  if (trainingData.empty ()) {
    cerr << endl << "No samples for training!" << endl;
    return;
  }
  WeightedLabeledData<RealVector, SharkLabelType> weightedTrainingData (trainingData, 1);
  trainer.train (classifier, weightedTrainingData);
  TreeStats::global.addTime (learningStats, duration_cast<duration<double> > (high_resolution_clock::now ()-start).count ());
}

// ================================================================================
template <typename PixelT>
void
Broceliande<PixelT>::predict (RFClassifier<SharkLabelType> &classifier) {

  /*! \brief PixelD interval de valeur de prédiction */
  typedef uint8_t PixelD;
  typedef uint8_t PixelGT;

  DEF_LOG ("Broceliande::predict", "");
  cout << Log::getLocalTimeStr () << " Start prediction" << endl;
  auto start = high_resolution_clock::now ();

  const OptGroundTruth &ogt (optBroceliande.optGroundTruth);
  const OptRanges &optTagValues (ogt.tagValues);
  const BroceliandeLabelType classCount (optTagValues.size ());
  vector<Raster<PixelD, 2> > outputClassRaster (1
#ifdef _BroceliandeRealVectorSharkLabelType
						+classCount
#endif
						, Raster<PixelD, 2> (size, PixelD (0)));

  vector<PixelGT> tagValues;
  tagValues.reserve (optTagValues.size ()+1);
  tagValues.push_back (isnan (ogt.bgValue) ? PixelGT (0) : PixelGT (ogt.bgValue));
  for (int v : optTagValues.getSet ())
    tagValues.push_back (PixelGT (v));

  DimImg pixelLeft = pixelCount;
  Data<RealVector> data;
  Data<SharkLabelType> prediction;
  
  DimImg pixelId = 0;
  const DimCore coreCount (optBroceliande.coreCount);
  for (; pixelId < pixelCount; ) {
    DimImg guessedBatchSize (min (DimImg (coreCount*optBroceliande.optTraining.maxPerRFBatch), pixelLeft));

    LOG ("count border");
    vector<DimImg> borderCounts (coreCount+1, 0);
    dealThread (guessedBatchSize, coreCount,
		[this, &borderCounts, &pixelId](const size_t &threadId, const DimImg &minVal, const DimImg &maxVal) {
		  DimImg &borderCount (borderCounts[threadId+1]);
		  DimImg localPixelId = pixelId+minVal;
		  for (DimImg idx = minVal; idx < maxVal; ++idx, ++localPixelId)
		    if (border.isBorder (localPixelId))
		      ++borderCount;
		});
    partial_sum (borderCounts.data (), borderCounts.data ()+coreCount+1, borderCounts.data ());
    for (size_t threadId = 0; threadId < coreCount+1; ++threadId)
      LOG ("borderCounts[" << (threadId) << "]: " << borderCounts [threadId]);

    LOG ("prediction");
    getData (data, pixelId, guessedBatchSize-borderCounts[coreCount]);
    prediction = classifier (data);

    LOG ("copy");
    dealThread (guessedBatchSize, coreCount,
		[this, &outputClassRaster, &borderCounts, &pixelId, &classCount, &tagValues, &prediction] (const size_t &threadId, const DimImg &minVal, const DimImg &maxVal) {
		  DimImg localPixelId = pixelId+minVal;
		  PixelGT *pixel (outputClassRaster[0].getPixels ()+localPixelId);
		  
		  for (DimImg idx = minVal, predIdx = minVal-borderCounts[threadId]; idx < maxVal; ++idx, ++localPixelId, ++pixel) {
		    if (border.isBorder (localPixelId))
		      continue;
#ifdef _BroceliandeRealVectorSharkLabelType
		    RealVector values (prediction.element (predIdx));
		    for (BroceliandeLabelType c (0); c < classCount; ++c) {
		      outputClassRaster[c+1].getPixels ()[localPixelId] = PixelD (values [c] * 255);
		      if (values [c] > .5)
			*pixel = tagValues [c+1];
		    }
#else
		    *pixel = tagValues [prediction.element (predIdx)];
#endif
		    ++predIdx;
		  }
		});

    LOG ("next");
    pixelId += guessedBatchSize;
    pixelLeft -= guessedBatchSize;
  }
  TreeStats::global.addTime (predictionStats, duration_cast<duration<double> > (high_resolution_clock::now ()-start).count ());

  if (noWriteFlag)
    return;

  outputImage.createImage (size, GDT_Byte, 1, inputImage, topLeft);
  writeBand (outputClassRaster[0].getPixels (), 0);
  outputImage.close ();

#ifdef _BroceliandeRealVectorSharkLabelType
  const string backupName (outputImage.getFileName ());
  const string outputBaseName  = boost::filesystem::path (backupName).replace_extension ("").string ();
  const string outputExtension  = boost::filesystem::extension (backupName);
  for (BroceliandeLabelType c (0); c < classCount; ++c) {
    ostringstream fileNameStream;
    fileNameStream << outputBaseName << "_class" << std::setfill ('0') << std::setw (3) << int (c) << outputExtension;
    outputImage.setFileName (fileNameStream.str ());
    outputImage.createImage (size, GDT_Byte, 1, inputImage, topLeft);
    writeBand (outputClassRaster[1+c].getPixels (), 0);
    outputImage.close ();
  }
  outputImage.setFileName (backupName);
#endif
}

// ================================================================================
template<typename PixelT>
void
Broceliande<PixelT>::saveOrig4Copy (const Raster<PixelT, 2> &raster, const DimChannel &inputId) {
  DEF_LOG ("Broceliande::saveOrig4Copy", "inputId: " << inputId);
  auto start = high_resolution_clock::now ();
  featuresOutput.pixelFeature.getOrig4Copy (inputId) = raster.getPixelsVector ();
  TreeStats::global.addTime (copyImageStats, duration_cast<duration<double> > (high_resolution_clock::now ()-start).count ());
}

template<typename PixelT>
void
Broceliande<PixelT>::saveOrig4Dap (const Raster<PixelT, 2> &raster, const DimChannel &inputId) {
  DEF_LOG ("Broceliande::saveOrig4Dap", "inputId: " << inputId);
  auto start = high_resolution_clock::now ();
  featuresOutput.pixelFeature.getOrig4Dap (inputId) = raster.getPixelsVector ();
  TreeStats::global.addTime (copyImageStats, duration_cast<duration<double> > (high_resolution_clock::now ()-start).count ());
}

// ================================================================================
template<typename PixelT>
template<typename FeatureT>
void
Broceliande<PixelT>::writeBand (FeatureT *pixels, const DimChannel &channel) {
  DEF_LOG ("Broceliande::writeBand", "channel: " << channel);
  auto start = high_resolution_clock::now ();
  outputImage.writeBand (pixels, channel);
  TreeStats::global.addTime (writeStats, duration_cast<duration<double> > (high_resolution_clock::now ()-start).count ());
}

// ================================================================================
template<typename PixelT>
void
Broceliande<PixelT>::setMatrixRowData (RealMatrix &inputs, const size_t &row, const DimImg &pixelId) const {
  DimChannel channel (0);
  featuresOutput.setMatrixRowData (inputs, channel, row, pixelId);

  // XXX Haar and Stat only 2D (C8 connectivity)
  Point<2> p = idx2point (size, pixelId);
  DimChannel haarId (0);
  for (const vector<TextWin<2> > &textWins : haars) {
    const double *integralImageP (integralImages [iiHaarIdx [haarId]].data ());
    DimChannel textWinId (0);
    for (const TextWin<2> &textWin : textWins) {
      double hx (textWin.getHx (integralImageP, p.coord[0], p.coord[1], pixelId));
      double hy (textWin.getHy (integralImageP, p.coord[0], p.coord[1], pixelId));
      inputs (row, channel++) = hx;	// hx+M/2 => pixel
      inputs (row, channel++) = hx+hy;	// (hx+hy)/2+M/2 => pixel
      inputs (row, channel++) = hy;	// hx+M/2 => pixel
      inputs (row, channel++) = hy-hx;	// (hx+hy)/2+M/2 => pixel
      ++textWinId;
    }
    ++haarId;
  }
  DimChannel statId (0);
  double mean, std, ent;
  for (const vector<TextWin<2> > &textWins : stats) {
    const double *integralImageP (integralImages [iiStatIdx [statId]].data ());
    const double *doubleIntegralImageP (doubleIntegralImages [diiStatIdx [statId]].data ());
    DimChannel textWinId (0);
    for (const TextWin<2> &textWin : textWins) {
      textWin.getMeanStdEnt (mean, std, ent, integralImageP, doubleIntegralImageP, p.coord[0], p.coord[1], pixelId);
      inputs (row, channel++) = mean;	// => pixel
      inputs (row, channel++) = std;	// => double
      inputs (row, channel++) = ent;	// => double
      ++textWinId;
    }
    ++statId;
  }
}

// ================================================================================
template<typename PixelT>
void
Broceliande<PixelT>::getSamples (LabeledData<RealVector, SharkLabelType> &data, const GroundTruth::SetType &setType) const {
  DEF_LOG ("Broceliande::getSamples", "");

  const DimImg sampleCount = groundTruth.getCount (setType);
  const DimChannel bandCount = optChannel.getOutputCount ();
  LOG ("sampleCount: " << sampleCount);
  if (sampleCount < 1)
    return;
  vector<size_t> batchSizes = shark::detail::optimalBatchSizes (sampleCount, getBatchSize (sampleCount));
  DimImg batchCount = batchSizes.size ();
  data = LabeledData<RealVector, SharkLabelType> (batchCount);

  const BroceliandeLabelType classCount (optBroceliande.optGroundTruth.tagValues.size ());
  for (size_t batchId = 0; batchId != batchCount; ++batchId) {
    //WeightedDataBatch<InputLabelBatch<RealMatrix, SharkLabelType>, RealVector>
    auto batch = data.batch (batchId);
    RealMatrix &inputs = batch.input;
    inputs.resize (batchSizes[batchId], bandCount);

#ifdef _BroceliandeRealVectorSharkLabelType
    RealMatrix
#else
      UIntVector
#endif
      &labels = batch.label;
#ifdef _BroceliandeRealVectorSharkLabelType
    labels.resize (batchSizes[batchId], classCount);
#else
    labels.resize (batchSizes[batchId]);
#endif
  }
  size_t dataId = 0, batchId = 0, dataIdInBatch = 0;
  DimImg nextBatch = 0;
  RealMatrix *pInputs = NULL;
#ifdef _BroceliandeRealVectorSharkLabelType
  RealMatrix
#else
    UIntVector
#endif
    *pLabels = NULL;
  groundTruth.forEachIdx (setType,
			  [this, &dataId, &batchId, &dataIdInBatch, &nextBatch, &batchSizes, &data, &pLabels, &classCount, &pInputs]
			  (const BroceliandeLabelType &tagClass, const DimImg &pixelId) {
			    if (dataId == nextBatch) {
			      pInputs = &data.batch (batchId).input;
			      pLabels = &data.batch (batchId).label;
			      nextBatch += batchSizes[batchId];
			      dataIdInBatch = 0;
			      ++batchId;
			    }
#ifdef _BroceliandeRealVectorSharkLabelType
			    for (BroceliandeLabelType c (0); c < classCount; ++c)
			      (*pLabels) (dataIdInBatch, c) = (tagClass -1)  == c ? 1 : 0;
#else
			    (*pLabels) [dataIdInBatch] = tagClass;
#endif

			    setMatrixRowData (*pInputs, dataIdInBatch, pixelId);
			    ++dataId;
			    ++dataIdInBatch;
			  });
}

// ================================================================================
template<typename PixelT>
DimImg
Broceliande<PixelT>::getData (Data<RealVector> &data, DimImg pixelId, const DimImg &maxDataLeft) const {
  DEF_LOG ("Broceliande::getData", "pixelCount: " << pixelCount << " borderCount: " << border.borderCount () << " pixelId : " << pixelId << " maxDataLeft: " << maxDataLeft);
  if (maxDataLeft < 1)
    return pixelId;

  const DimChannel bandCount = optChannel.getOutputCount ();
  LOG ("bandCount: " << bandCount);

  vector<size_t> batchSizes = shark::detail::optimalBatchSizes (maxDataLeft, getBatchSize (maxDataLeft));
  DimImg batchCount = batchSizes.size ();
  data = Data<RealVector> (batchCount);

  for (size_t b = 0; b != batchCount; ++b) {
    RealMatrix& inputs = data.batch (b);
    inputs.resize (batchSizes[b], bandCount);
    for (size_t dataCount (0); dataCount != batchSizes[b]; ++pixelId) {
      if (border.isBorder (pixelId))
	continue;
      setMatrixRowData (inputs, dataCount, pixelId);
      ++dataCount;
    }
  }
  return pixelId;
}

// ================================================================================
template<typename PixelT>
template <typename PixelGT>
bool
Broceliande<PixelT>::chooseGroundTruth (IImage<2> &groundTruthImage, const PixelGT &bgValue) {
  DEF_LOG ("Broceliande::chooseGroundTruth",  "tagValues: " << optBroceliande.optGroundTruth.tagValues << " bgValue: " << bgValue << " pixelCount: " << pixelCount << " topLeft: " << topLeft << " size: " << size);
  const OptGroundTruth &ogt (optBroceliande.optGroundTruth);
  const bool isBgTag = !isnan (ogt.bgValue);
  const OptRanges &optTagValues (ogt.tagValues);

  Raster<PixelGT, 2> gtRaster (size);
  const Border<2> &border (this->border);
  LOG ("bgTagRate: " << ogt.bgTagRate << " bgRate: " << ogt.bgRate);
  BroceliandeLabelType classCount (optTagValues.size ());

  vector<PixelGT> tagValues;
  tagValues.reserve (classCount);
  for (int v : optTagValues.getSet ())
    tagValues.push_back (PixelGT (v));

  const PixelGT bgPixelValue (isBgTag ? PixelGT (bgValue) : tagValues [0]);
  groundTruthImage.readBand<PixelGT> (gtRaster, 0, topLeft, size);
  const PixelGT *gtPixels (gtRaster.getPixels ());
  groundTruth.setTag (pixelCount, classCount,
		      [&border, &gtPixels, &tagValues, &bgPixelValue] (const DimImg &idx, BroceliandeLabelType &classIdx) {
			classIdx = 0;
			if (border.isBorder (idx))
			  return false;
			typename vector<PixelGT>::iterator low = lower_bound (tagValues.begin (), tagValues.end (), gtPixels [idx]);
			if (*low == gtPixels [idx]) {
			  classIdx = 1+(low - tagValues.begin ());
			  return true;
			}
			return gtPixels [idx] == bgPixelValue;
		      });

  tagValues.insert (upper_bound (tagValues.begin (), tagValues.end (), bgPixelValue), bgPixelValue);
  groundTruth.chooseBgRnd (pixelCount, border.borderCount (), ogt.bgTagRate, ogt.bgRate,
			   [&gtPixels, &tagValues, &border] (const DimImg &idx) {
			     if (border.isBorder (idx))
			       return false;
			     typename vector<PixelGT>::iterator low = lower_bound (tagValues.begin (), tagValues.end (), gtPixels [idx]);
			     return low != tagValues.end () && *low != gtPixels [idx];
			   });
  return groundTruth.getCount (GroundTruth::tagSet);
}

// ================================================================================
template<typename PixelT>
int
Broceliande<PixelT>::chooseSamples () {
  DEF_LOG ("Broceliande::chooseSamples", "");
  cleanTags ();
  groundTruth.chooseSamples (optBroceliande.optGroundTruth.tagsRate, optBroceliande.optGroundTruth.bgRate, optBroceliande.optGroundTruth.bgTagRate);
  cleanSamples ();
  // XXX cout << Log::getLocalTimeStr () << " Find samples: " << groundTruth.getCount (GroundTruth::fg1ISet) << " foreground, " << groundTruth.getCount (GroundTruth::bgSet) << " background" << endl;
  return groundTruth.getCount (GroundTruth::sampleSet);
}

// ================================================================================
template<typename PixelT>
void
Broceliande<PixelT>::showGroundTruth (const string &filename, const IImage<2> &groundTruthImage) const {
  groundTruth.showGroundTruth (border, filename, groundTruthImage, topLeft);
}

// ================================================================================
template<typename PixelT>
DimImg
Broceliande<PixelT>::getBatchSize (const DimImg &maxDataLeft) const {
  DimImg result =  DimImg (
			   max (size_t (100),
				min (size_t (optBroceliande.optTraining.maxPerRFBatch), size_t (maxDataLeft))
				/ optBroceliande.coreCount));
  return result;
}

// ================================================================================
template<typename PixelT>
void
Broceliande<PixelT>::cleanTags () {
  if (!optBroceliande.optTraining.cleanTagsFlag)
    return;
  DEF_LOG ("Broceliande::cleanTags", "pixelCount: " << pixelCount);
  auto start = high_resolution_clock::now ();

  groundTruth.chooseSamples (OptRate ("50%"), OptRate ("50%"), OptRate ("50%"));
  RFTrainer<SharkLabelType> trainer (false, false);
  RFClassifier<SharkLabelType> classifierForCleanTags;
  LabeledData<RealVector, SharkLabelType> trainingData;
  trainer.setNTrees (optBroceliande.optTraining.nTrees);
  trainer.setMTry (sqrt (optChannel.getOutputCount ()));
  getSamples (trainingData, GroundTruth::sampleSet);
  WeightedLabeledData<RealVector, SharkLabelType> weightedTrainingData (trainingData, 1);
  trainer.train (classifierForCleanTags, weightedTrainingData);

  LabeledData<RealVector, SharkLabelType> testingData;
  getSamples (testingData, GroundTruth::tagSet);
  const Data<SharkLabelType> &predictionTest (classifierForCleanTags (testingData.inputs ()));
  const Data<SharkLabelType> &labels (testingData.labels ());

#ifdef _BroceliandeRealVectorSharkLabelType
  const BroceliandeLabelType classCount (optBroceliande.optGroundTruth.tagValues.size ());
#endif
  DimImg tagsCount = groundTruth.getCount (GroundTruth::tagSet);
  vector<DimImg> badTags;
  badTags.reserve (tagsCount*0.1);
  for (DimImg predIdx = 0; predIdx < tagsCount; ++predIdx) {
#ifdef _BroceliandeRealVectorSharkLabelType
    RealVector values (predictionTest.element (predIdx));
    RealVector ref (labels.element (predIdx));
    for (BroceliandeLabelType c (0); c < classCount; ++c)
      if (ref [c]) {
	if (values [c] < .5)
	  badTags.push_back (predIdx);
	break;
      } else if (values [c] > .5) {
	badTags.push_back (predIdx);
	break;
      }
#else
    if (predictionTest.element (predIdx) != labels.element (predIdx))
      badTags.push_back (predIdx);
#endif
  }
  cout << Log::getLocalTimeStr () << " remove badTags: " << badTags.size () << endl;
  groundTruth.cleanTag (badTags);
  TreeStats::global.addTime (cleanStats, duration_cast<duration<double> > (high_resolution_clock::now ()-start).count ());
}

template<typename PixelT>
void
Broceliande<PixelT>::cleanSamples () {
  if (!optBroceliande.optTraining.cleanSamplesFlag)
    return;
  DEF_LOG ("Broceliande::cleanSamples", "");
  //groundTruth.chooseBg (optBroceliande.optGroundTruth.bgRate, optBroceliande.optGroundTruth.bgTagRate);
  auto start = high_resolution_clock::now ();

  RFTrainer<SharkLabelType> trainer (false, false);
  RFClassifier<SharkLabelType> classifierForCleanSamples;
  LabeledData<RealVector, SharkLabelType> trainingData;
  trainer.setNTrees (optBroceliande.optTraining.nTrees);
  trainer.setMTry (sqrt (optChannel.getOutputCount ()));
  getSamples (trainingData, GroundTruth::sampleSet);
  WeightedLabeledData<RealVector, SharkLabelType> weightedTrainingData (trainingData, 1);
  trainer.train (classifierForCleanSamples, weightedTrainingData);

  LabeledData<RealVector, SharkLabelType> testingData;
  getSamples (testingData, GroundTruth::sampleSet);
  const Data<SharkLabelType> &predictionTest (classifierForCleanSamples (testingData.inputs ()));
  const Data<SharkLabelType> &labels (testingData.labels ());

  DimImg samplesCount = groundTruth.getCount (GroundTruth::sampleSet);
  vector<DimImg> badSamples;
  badSamples.reserve (samplesCount*0.2);
#ifdef _BroceliandeRealVectorSharkLabelType
  const BroceliandeLabelType classCount (optBroceliande.optGroundTruth.tagValues.size ());
#endif
  for (DimImg predIdx = 0; predIdx < samplesCount; ++predIdx) {
#ifdef _BroceliandeRealVectorSharkLabelType
    RealVector values (predictionTest.element (predIdx));
    RealVector ref (labels.element (predIdx));
    for (BroceliandeLabelType c (0); c < classCount; ++c)
      if (ref [c]) {
	if (values [c] < .5)
	  badSamples.push_back (predIdx);
	break;
      } else if (values [c] > .5) {
	badSamples.push_back (predIdx);
	break;
      }
#else
    if (predictionTest.element (predIdx) != labels.element (predIdx))
      badSamples.push_back (predIdx);
#endif
  }
  cout << Log::getLocalTimeStr () << " remove badSamples: " << badSamples.size () << endl;
  groundTruth.cleanSamples (badSamples);
  TreeStats::global.addTime (cleanStats, duration_cast<duration<double> > (high_resolution_clock::now ()-start).count ());
}


// ================================================================================
