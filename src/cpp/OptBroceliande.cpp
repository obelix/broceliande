////////////////////////////////////////////////////////////////////////////
// Copyright IRISA 2017							  //
// 									  //
// triskele.obelix (at) irisa.fr					  //
// 									  //
// This  software is  a computer  program  whose purpose  is to  classify //
// pixels by using TRISKELE and random forest.				  //
// 									  //
// This software is governed by the CeCILL-B license under French law and //
// abiding by  the rules of distribution  of free software. You  can use, //
// modify  and/or  redistribute  the  software under  the  terms  of  the //
// CeCILL-B license as circulated by CEA, CNRS and INRIA at the following //
// URL "http://www.cecill.info".					  //
// 									  //
// As a counterpart to the access to  the source code and rights to copy, //
// modify and  redistribute granted  by the  license, users  are provided //
// only with a limited warranty and  the software's author, the holder of //
// the economic  rights, and the  successive licensors have  only limited //
// liability.								  //
// 									  //
// In this respect, the user's attention is drawn to the risks associated //
// with loading,  using, modifying  and/or developing or  reproducing the //
// software by the user in light of its specific status of free software, //
// that may  mean that  it is  complicated to  manipulate, and  that also //
// therefore means  that it  is reserved  for developers  and experienced //
// professionals having in-depth computer  knowledge. Users are therefore //
// encouraged  to load  and test  the software's  suitability as  regards //
// their  requirements  in  conditions  enabling the  security  of  their //
// systems and/or  data to  be ensured  and, more  generally, to  use and //
// operate it in the same conditions as regards security.		  //
// 									  //
// The fact that  you are presently reading this means  that you have had //
// knowledge of the CeCILL-B license and that you accept its terms.	  //
////////////////////////////////////////////////////////////////////////////

#define LAST_VERSION "2.8 2021-11-11 multiclasses ("+getOsName ()+")"

#include <boost/thread.hpp>
#include <boost/algorithm/string.hpp>
#include <boost/filesystem.hpp>
#include <boost/program_options/options_description.hpp>
#include <boost/program_options/parsers.hpp>
#include <boost/program_options/variables_map.hpp>

#include "misc.hpp"
#include "obelixPantex.hpp"
#include "obelixGeo.hpp"
#include "Appli/OptCropParser.hpp"
#include "Appli/OptChannelParser.hpp"

#include "OptBroceliande.hpp"
#include "OptGroundTruthParser.hpp"
#include "OptTrainingParser.hpp"
#include "OptBatchParser.hpp"

using namespace std;
using namespace boost;
using namespace boost::program_options;
using namespace boost::filesystem;

using namespace obelix;
using namespace obelix::broceliande;

namespace po = boost::program_options;

#ifndef OBELIX_ERROR
#define OBELIX_ERROR(expr) std::cerr << expr << std::endl << std::flush, std::exit (1)
#endif


// ================================================================================
OptBroceliande::OptBroceliande ()
  : debugFlag (false),
    timeFlag (false),
    countFlag (false),
    countingSortCeil (2),
    classificationFlag (false),
    includeNoDataFlag (false),
    noDataValue (NAN),
    pantexWindowSide (35),
    connectivity (Connectivity::C4),
    coreCount (boost::thread::hardware_concurrency ()),
    threadPerImage (max (DimCore (1), coreCount/4)),
    tilePerThread (max (DimCore (1), threadPerImage*2)),
    seqTileFlag (false),
    autoThreadFlag (false),    
    memImpactFlag (false) {
}

OptBroceliande::OptBroceliande (int argc, char** argv)
  : OptBroceliande () {
  parse (argc, argv);
}

// ================================================================================
OptCropParser optCropParser;
OptChannelParser optChannelParser;
OptGroundTruthParser optGroundTruthParser;
OptTrainingParser optTrainingParser;
OptBatchParser optBatchParser;

static po::options_description mainDescription ("Main options", getCols ());
static po::options_description hide ("Hidded options", getCols ());
static char *prog = NULL;

// ================================================================================
void
OptBroceliande::usage (const string &msg, const bool &hidden) const {
  if (!msg.empty ())  {
    cout << msg << endl;
    exit (1);
  }
  cout << endl
       <<"Usage: " << endl
       <<"  A)   " << prog << " -i in.tif -o out.tif   [options]" << endl
       <<"  B)   " << prog << " -i in.tif -o out.tif   [options] -g gt.tif" << endl
       <<"  C)   " << prog << " -i in.tif [-o out.tif] [options] -g gt.tif --saveTrain rf.txt" << endl
       <<"  D)   " << prog << " -i in.tif -o out.tif   [options] --loadTrain rf.txt" << endl
       <<"  E)   " << prog << " -batchDir rep/         [options] --loadTrain rf.txt" << endl
       << endl
       <<"  A: copy, produce AP or produce texture" << endl
       <<"  B: prediction" << endl
       <<"  C: save training [and prediction]" << endl
       <<"  D: load training and prediction" << endl
       <<"  E: load training and prediction with set of images" << endl << endl
       << endl << mainDescription
       << endl << optCropParser.description
       << endl << optChannelParser.description
       << endl << optGroundTruthParser.description
       << endl << optTrainingParser.description
       << endl << optBatchParser.description
       << endl;
  if (hidden)
    cout << hide << endl;
  exit (0);
}

void
OptBroceliande::version () const {
  cout << endl << prog << " version " << LAST_VERSION << endl << endl
       << "  Triskele  : efficiant tree builder (https://sourcesup.renater.fr/projects/triskele)" << endl
       << "  GDAL      : read and write image (http://www.gdal.org/)" << endl
       << "  cct       : inspired by jirihavel library to build trees (https://github.com/jirihavel/libcct)" << endl
       << "  Boost     : C++ libraries (http://www.boost.org/)" << endl << endl
       << "  This software is a Obelix team production (http://www-obelix.irisa.fr/)" << endl << endl;
  exit (0);
}

// ================================================================================
static const string inputFile = "input-file";
static const char *const inputFileC = inputFile.c_str ();

void
OptBroceliande::parse (int argc, char** argv) {
  DEF_LOG ("OptBroceliande::parse", "");
  prog = argv [0];
  try {

    bool helpFlag = false, versionFlag = false, useTheForceLuke = false;
    string inputName, outputName;
    mainDescription.add_options ()
      ("help", po::bool_switch (&helpFlag), "produce this help message")
      ("version", po::bool_switch (&versionFlag), "display version information")

      ("timeFlag", po::bool_switch (&timeFlag), "give execution time")
      ("countFlag", po::bool_switch (&countFlag), "give pixels and nodes count")
      ("countingSortCeil", po::value<size_t> (&countingSortCeil)->default_value (countingSortCeil), "force counting sort under ceil (n.b. value in [0..2^16] => 300MB!)")
      ("showConfig", po::value<string> (&showConfig), "show actual configuration on a file")

      ("includeNoDataFlag", po::bool_switch (&includeNoDataFlag), "build tree with all pixels (included no-data)")
      ("noDataValue", po::value<double> (&noDataValue)->default_value (noDataValue), "value use as nodata in all bands")

      ("input,i", po::value<string> (&inputName), "input file name image")
      ("output,o", po::value<string> (&outputName), "output file name for hyperbands image contains attributs profiles (or classification image with -g options)")
      ("connectivity", po::value<Connectivity> (&connectivity)->default_value (connectivity), "connectivity")
      ("coreCount", po::value<DimCore> (&coreCount)->default_value (coreCount), "thread used for parallel process")
      ("threadPerImage", po::value<DimCore> (&threadPerImage)->default_value (threadPerImage), "thread per Image to build tree")
      ("tilePerThread", po::value<DimCore> (&tilePerThread)->default_value (tilePerThread), "tiles per threads")
      ("seqTileFlag", po::bool_switch (&seqTileFlag), "force Tile sequential computation")
      ("autoThreadFlag", po::bool_switch (&autoThreadFlag), "auto tune tilePerThread count")
      ("memImpactFlag", po::bool_switch (&memImpactFlag), "estimate memory impact")
      ;

    hide.add_options ()
      ("useTheForceLuke", po::bool_switch (&useTheForceLuke), "display hidded options")
      ("debug", po::bool_switch (&debugFlag), "debug mode")
      ("pantexWindowSide", po::value<DimPantexOcc> (&pantexWindowSide)->default_value (pantexWindowSide), "side length of the sliding window to compute Pantex index (min 5, max 127)")
      ;

    po::options_description cmd ("All options");
    cmd.add (mainDescription).add (optCropParser.description).add (optChannelParser.description).
      add (optGroundTruthParser.description).add (optTrainingParser.description).add (optBatchParser.description).add (hide).add_options ()
      (inputFileC, po::value<vector<string> > (), "input output")
      ;

    po::positional_options_description p;
    p.add (inputFileC, -1);
    po::variables_map vm;
    po::basic_parsed_options<char> parsed = po::command_line_parser (argc, argv).options (cmd).positional (p).run ();
    store (parsed, vm);
    po::notify (vm);

    if (debugFlag) {
#ifndef ENABLE_LOG
      cout << "No debug option available (was compiled with -DENABLE_LOG)" << endl;
#endif
    }
    Log::debug = debugFlag;
    if (useTheForceLuke)
      usage ("", true);
    if (versionFlag)
      version ();
    if (helpFlag)
      usage ();

    if (vm.count (inputFileC)) {
      vector<string> var = vm[inputFileC].as<vector<string> > ();
      int nbArgs = vm[inputFileC].as<vector<string> > ().size ();
      int setArgs = 0;
      if (setArgs < nbArgs && inputName.empty ())
	inputName = var [setArgs++];
      if (setArgs < nbArgs && outputName.empty ())
	outputName = var [setArgs++];
      if (nbArgs > setArgs)
	usage ("Too much arguments");
    }
    // XXX revoir le nombre d'arguments si seulement config

    if (inputName.empty () && optBatchParser.batchDir.empty ())
      usage ("No input file(s)");

    if (!optBatchParser.batchDir.empty () &&
	(optTrainingParser.loadTrainName.empty () && optGroundTruthParser.fileName.empty ()))
      usage ("No training");

    if (!optTrainingParser.loadTrainName.empty () && !optTrainingParser.saveTrainName.empty ())
      usage ("Can't load and save in same time");

    if ((!outputName.empty () ||
	 !optGroundTruthParser.fileName.empty () ||
	 !optTrainingParser.saveTrainName.empty () ||
	 (optTrainingParser.loadTrainName.empty () && optTrainingParser.saveTrainName.empty ()))
	&& inputName.empty ())
      usage ("No inputFileName (-i)");

    if (!optTrainingParser.loadTrainName.empty () && !optGroundTruthParser.fileName.empty ())
      usage ("Can't load and training in same time");

    if (!optTrainingParser.saveTrainName.empty () && optGroundTruthParser.fileName.empty ())
      usage ("Can't save without training (--groundTruth)");

    if (!optTrainingParser.loadTrainName.empty () &&
	(inputName.empty () != outputName.empty ()))
      usage ("Load with inputFileName and not outputName (or the opposite)");

    inputImage.setFileName (inputName);
    outputImage.setFileName (outputName);
    optChannelParser.parse (optChannel, inputName, parsed);
    DimChannel spectralDepth (0);
    bool mixedBandFlag (false);
    inputImage.readImage (spectralDepth, mixedBandFlag);
    optCropParser.parse (optCrop, inputImage.getSize (), parsed);
    optGroundTruthParser.parse (optGroundTruth, parsed);
    optTrainingParser.parse (optTraining, parsed);

    // XXX si pas d'image
    if (inputImage.isEmpty ())
      OBELIX_ERROR ("Can't parse empty image");
    cout << Log::getLocalTimeStr () << " " << inputImage << endl << flush;

    Size<2> imgSize = inputImage.getSize ();
    // XXX a revoir si 3D
    checkBaseType (imgSize.side[0], imgSize.side[1], connectivity);
    GDALDataType inputType = inputImage.getDataType ();
    if (inputType == GDT_Unknown)
      OBELIX_ERROR ("Can't parse unknown or inconsistant bands");

    GDALDataType groundTruthType = GDT_Unknown;
    if (!optGroundTruth.image.isEmpty ()) {
      Size<2> gtSize = optGroundTruth.image.getSize ();

      cout << Log::getLocalTimeStr () << " " << optGroundTruth.image << endl << flush;

      if (imgSize.side[0] != gtSize.side[0] || imgSize.side[1] != gtSize.side[1])
	OBELIX_ERROR (optGroundTruthParser.fileName << ":" << imgSize << " and groundTruth" << gtSize << " size differ ");
      if (optGroundTruth.image.getBandCount () != 1)
	OBELIX_ERROR ("GroundTruth bandCount must have one band (1 != " << optGroundTruth.image.getBandCount () << ")");
      groundTruthType = optGroundTruth.image.getDataType ();
      if (groundTruthType == GDT_Unknown)
	OBELIX_ERROR ("Can't parse read unknown band");
      classificationFlag = true;
    } else {
      // XXX
      // no showGroundTruth
      // no recall
      // no accuracy
    }

    if (optCrop.topLeft.coord[0] || optCrop.topLeft.coord[1] ||
	optCrop.size.side[0] != imgSize.side[0] || optCrop.size.side[1] != imgSize.side[1])
      cout << Log::getLocalTimeStr () << " crop:" << optCrop << endl;

    if (!optBatch.batchDir.empty ()) {
      cout << Log::getLocalTimeStr () << " batch from:" << (optBatch.batchDir / "*").string () << endl
       	   << Log::getLocalTimeStr () << " batch to:" << (optBatch.batchResultDir / (optBatch.batchPrefixResult + "*")).string () << endl;
      if (!classificationFlag)
	OBELIX_ERROR ("batch without classification");
      path batchPath (optBatch.batchDir);
      if (!exists (batchPath) || !is_directory (batchPath))
	OBELIX_ERROR (optBatch.batchDir << " is not a source batch directory");
      path resultPath (optBatch.batchResultDir);
      if (!exists (resultPath) || !is_directory (resultPath))
	OBELIX_ERROR (optBatch.batchResultDir << " is not a result batch directory");
    }

    if (pantexWindowSide < 5 || pantexWindowSide > 127)
      throw invalid_argument ("invalid pantexWindowSide");

    if (memImpactFlag) {
      MemoryEstimation memoryEstimation (optCrop.size, inputImage.getDataType (), connectivity, countingSortCeil, coreCount, .40, false);
      optChannel.memoryImpact (memoryEstimation);
      // XXX si GT ajouter la prediction
      printMem (cerr);
      cerr << memoryEstimation;
    }
  } catch (std::exception& e) {
    cerr << "error: " << e.what () << endl;
    usage ("Bad options");
  }

  if (!showConfig.empty ()) {
    out.open (showConfig, ios_base::out);
    out
      << endl
      << Log::getLocalTimeStr () << " Broceliande " << LAST_VERSION << " config:" << endl << endl
      << *this << endl;
  }
}

// ================================================================================
ostream &
obelix::broceliande::operator << (ostream &out, const OptBroceliande &optBroceliande) {
  unsigned int leftMarging = 25;
  return out
    SHOW_OPTION_FLAG ("--timeFlag", optBroceliande.timeFlag)
    SHOW_OPTION_FLAG ("--countFlag", optBroceliande.countFlag)
    SHOW_OPTION ("--countingSortCeil", optBroceliande.countingSortCeil)
    SHOW_OPTION ("--showConfig", optBroceliande.showConfig)
    SHOW_OPTION_FLAG ("--classificationFlag", optBroceliande.classificationFlag)
    SHOW_OPTION_FLAG ("--pantexWindowSide", optBroceliande.pantexWindowSide)
    SHOW_OPTION_FLAG ("--includeNoDataFlag", optBroceliande.includeNoDataFlag)
    SHOW_OPTION ("--noDataValue", optBroceliande.noDataValue)

    SHOW_OPTION ("--input", optBroceliande.inputImage)
    SHOW_OPTION ("--output", optBroceliande.outputImage)
    SHOW_OPTION ("--connectivity", optBroceliande.connectivity << " (" << int (optBroceliande.connectivity) << ")")
    SHOW_OPTION ("--coreCount", optBroceliande.coreCount)
    SHOW_OPTION ("--threadPerImage", optBroceliande.threadPerImage)
    SHOW_OPTION ("--tilePerThread", optBroceliande.tilePerThread)
    SHOW_OPTION_FLAG ("--seqTileFlag", optBroceliande.seqTileFlag)
    SHOW_OPTION_FLAG ("--autoThreadFlag", optBroceliande.autoThreadFlag)

    << endl
    << "OptCrop: " << endl << optBroceliande.optCrop
    << "OptChannel: " << endl << optBroceliande.optChannel
    << "OptGroundTruth: " << endl << optBroceliande.optGroundTruth
    << "OptTraining: " << endl << optBroceliande.optTraining
    << "OptBatch: " << endl << optBroceliande.optBatch
    << flush;
}

// ================================================================================
void
obelix::broceliande::checkBaseType (uint64_t width, uint64_t height, Connectivity c) {
  if (!width || !height) {
    // no size
    return;
  }
  size_t dimSideImgCard = sizeof (DimSideImg)*8;
  size_t dimImgCard = sizeof (DimImg)*8;
  size_t dimImg_MAX_Card = size64 (DimImg_MAX);
  size_t dimNodeIdxCard = sizeof (DimNode)*8;
  size_t dimEdgeCard = sizeof (DimEdge)*8;

  size_t widthCard = size64 (width);
  size_t heightCard = size64 (height);
  size_t cFactor = 2+((c&C6P) != 0)+((c&C6N) != 0);
  uint64_t imageSize = width*height;
  size_t imageCard = size64 (imageSize);

  if (dimSideImgCard >= widthCard && dimSideImgCard >= heightCard &&
      dimImgCard >= imageCard &&
      dimImgCard == dimImg_MAX_Card &&
      dimNodeIdxCard >= (1+imageCard) &&
      dimEdgeCard >= size64 (imageSize*cFactor))
    return;

  cerr << "!!! Warning !!!" << endl
       << "Unsafe type for image " << width << "x" << height << "=" << imageSize
       << " (bits size " << widthCard << "x" << heightCard << "=" << imageCard << ")" << endl
       << "  DimSideImg (" << dimSideImgCard << ") >=? max (" << widthCard << ", " << heightCard << ")" << endl
       << "  DimImg     (" << dimImgCard << ") >=? (" << imageCard << ")" << endl
       << "  DimImg     (" << dimImgCard << ") ==? " << DimImg_MAX << " (" << dimImg_MAX_Card << ")" << endl
       << "  DimNodeIdx (" << dimNodeIdxCard << ") >=? 2*" << imageSize << " (" << (1+imageCard) << ")" << endl
       << "  DimEdge    (" << dimEdgeCard    << ") >=? " << cFactor << "*" << imageSize << " (" << size64 (imageSize*cFactor)
       << ") " << c << endl;
  BOOST_ASSERT (false);
}

// ================================================================================
