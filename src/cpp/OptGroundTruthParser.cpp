////////////////////////////////////////////////////////////////////////////
// Copyright IRISA 2017							  //
// 									  //
// triskele.obelix (at) irisa.fr					  //
// 									  //
// This  software is  a computer  program  whose purpose  is to  classify //
// pixels by using TRISKELE and random forest.				  //
// 									  //
// This software is governed by the CeCILL-B license under French law and //
// abiding by  the rules of distribution  of free software. You  can use, //
// modify  and/or  redistribute  the  software under  the  terms  of  the //
// CeCILL-B license as circulated by CEA, CNRS and INRIA at the following //
// URL "http://www.cecill.info".					  //
// 									  //
// As a counterpart to the access to  the source code and rights to copy, //
// modify and  redistribute granted  by the  license, users  are provided //
// only with a limited warranty and  the software's author, the holder of //
// the economic  rights, and the  successive licensors have  only limited //
// liability.								  //
// 									  //
// In this respect, the user's attention is drawn to the risks associated //
// with loading,  using, modifying  and/or developing or  reproducing the //
// software by the user in light of its specific status of free software, //
// that may  mean that  it is  complicated to  manipulate, and  that also //
// therefore means  that it  is reserved  for developers  and experienced //
// professionals having in-depth computer  knowledge. Users are therefore //
// encouraged  to load  and test  the software's  suitability as  regards //
// their  requirements  in  conditions  enabling the  security  of  their //
// systems and/or  data to  be ensured  and, more  generally, to  use and //
// operate it in the same conditions as regards security.		  //
// 									  //
// The fact that  you are presently reading this means  that you have had //
// knowledge of the CeCILL-B license and that you accept its terms.	  //
////////////////////////////////////////////////////////////////////////////

#include <stdexcept>
#include <boost/program_options.hpp>

#include "obelixDebug.hpp"
#include "misc.hpp"

#include "OptGroundTruthParser.hpp"

using namespace std;
using namespace boost;
using namespace boost::program_options;

using namespace obelix::broceliande;

namespace po = boost::program_options;

// ================================================================================

OptGroundTruthParser::OptGroundTruthParser ()
  : tagValues ("1"),
    bgValue (NAN),
    tagsRate ("100%"),
    bgTagRate ("80%"),
    bgRate ("125%"),
    description ("GroundTruth options", getCols ()) {
    DEF_LOG ("OptGroundTruthParser::OptGroundTruthParser", "");

  description.add_options ()
    ("groundTruth,g", po::value<string> (&fileName), "ground truth file name image for classification")
    ("showGroundTruth", po::value<string> (&show), "show selected ground truth")

    ("tagValues", po::value<OptRanges> (&tagValues)->default_value (tagValues), "classes tag values  in ground truth")
    ("bgValue", po::value<double> (&bgValue)->default_value (bgValue), "value use as non background in ground truth (NAN => no bg tag)")
    ("tagsRate", po::value<OptRate> (&tagsRate)->default_value (tagsRate), "number for each classes: 200 means 200 pixels, 40% means 40% for each classes")
    ("bgTagRate", po::value<OptRate> (&bgTagRate)->default_value (bgTagRate), "number of tagged background of ground truth (the other are taken randomly)")
    ("bgRate", po::value<OptRate> (&bgRate)->default_value (bgRate), "number of background of ground truth rate of foreground")
    ;
}

// ================================================================================
void
OptGroundTruthParser::parse (OptGroundTruth &optGroundTruth, po::basic_parsed_options<char> &parsed) {
  DEF_LOG ("OptGroundTruthParser::parse", "");

  // XXX tagValues.setLimits (x, y);
  tagValues.toSet ();
  tagsRate.check ("tagsRate", true);
  bgTagRate.check ("bgTagRate", true);
  bgRate.check ("bgRate", false);

  optGroundTruth.image.setFileName (fileName);
  optGroundTruth.show = show;
  // XXX check tagValues.size () > 0
  optGroundTruth.tagValues = tagValues;
  optGroundTruth.bgValue = bgValue;
  optGroundTruth.tagsRate = tagsRate;
  optGroundTruth.bgTagRate = bgTagRate;
  optGroundTruth.bgRate = bgRate;

  if (!fileName.empty ())
    optGroundTruth.image.readImage ();
}

// ================================================================================
