////////////////////////////////////////////////////////////////////////////
// Copyright IRISA 2017							  //
// 									  //
// triskele.obelix (at) irisa.fr					  //
// 									  //
// This  software is  a computer  program  whose purpose  is to  classify //
// pixels by using TRISKELE and random forest.				  //
// 									  //
// This software is governed by the CeCILL-B license under French law and //
// abiding by  the rules of distribution  of free software. You  can use, //
// modify  and/or  redistribute  the  software under  the  terms  of  the //
// CeCILL-B license as circulated by CEA, CNRS and INRIA at the following //
// URL "http://www.cecill.info".					  //
// 									  //
// As a counterpart to the access to  the source code and rights to copy, //
// modify and  redistribute granted  by the  license, users  are provided //
// only with a limited warranty and  the software's author, the holder of //
// the economic  rights, and the  successive licensors have  only limited //
// liability.								  //
// 									  //
// In this respect, the user's attention is drawn to the risks associated //
// with loading,  using, modifying  and/or developing or  reproducing the //
// software by the user in light of its specific status of free software, //
// that may  mean that  it is  complicated to  manipulate, and  that also //
// therefore means  that it  is reserved  for developers  and experienced //
// professionals having in-depth computer  knowledge. Users are therefore //
// encouraged  to load  and test  the software's  suitability as  regards //
// their  requirements  in  conditions  enabling the  security  of  their //
// systems and/or  data to  be ensured  and, more  generally, to  use and //
// operate it in the same conditions as regards security.		  //
// 									  //
// The fact that  you are presently reading this means  that you have had //
// knowledge of the CeCILL-B license and that you accept its terms.	  //
////////////////////////////////////////////////////////////////////////////

#ifndef _Obelix_Broceliande_OptGroundTruth_hpp
#define _Obelix_Broceliande_OptGroundTruth_hpp

#include <iostream>

#include "IImage.hpp"
#include "Appli/OptRanges.hpp"
#include "Appli/OptRate.hpp"

namespace obelix {
  namespace broceliande {
    using namespace obelix::triskele;

    // ================================================================================
    class OptGroundTruth {
    public:

      /*!
       * \brief groundTruthImage vérité terrain partielle. En cas de présence elle déclanche la prédiction (sinon les couches filtrées sont produite)
       */
      IImage<2> image;
      /*!
       * \brief showGroundTruth nom du fichier de trace des échantillons de références
       */
      string show;

      /*!
       * \brief tagsValue classes dans l'image de référence.
       */
      OptRanges tagValues;
      double bgValue;

      /*!
       * \brief fgRate taux de sélection des FG étiquetés
       */
      OptRate tagsRate;
      /*!
       * \brief bgTagRate taux de sélection des BG étiquetés par rapport aux BG tirés de façon aléatoire
       */
      OptRate bgTagRate;
      /*!
       * \brief bgRate taux de sélection des BG de référence (étiquetés et aléatoires) par rapport aux FG sélectionnés
       */
      OptRate bgRate;

      OptGroundTruth ();

    };
    ostream &operator << (ostream &out, const OptGroundTruth & optGroundTruth);

    // ================================================================================
  } // broceliande
} // obelix

#endif //_Obelix_Broceliande_OptGroundTruth_hpp
