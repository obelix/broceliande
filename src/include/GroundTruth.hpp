////////////////////////////////////////////////////////////////////////////
// Copyright IRISA 2017							  //
// 									  //
// triskele.obelix (at) irisa.fr					  //
// 									  //
// This  software is  a computer  program  whose purpose  is to  classify //
// pixels by using TRISKELE and random forest.				  //
// 									  //
// This software is governed by the CeCILL-B license under French law and //
// abiding by  the rules of distribution  of free software. You  can use, //
// modify  and/or  redistribute  the  software under  the  terms  of  the //
// CeCILL-B license as circulated by CEA, CNRS and INRIA at the following //
// URL "http://www.cecill.info".					  //
// 									  //
// As a counterpart to the access to  the source code and rights to copy, //
// modify and  redistribute granted  by the  license, users  are provided //
// only with a limited warranty and  the software's author, the holder of //
// the economic  rights, and the  successive licensors have  only limited //
// liability.								  //
// 									  //
// In this respect, the user's attention is drawn to the risks associated //
// with loading,  using, modifying  and/or developing or  reproducing the //
// software by the user in light of its specific status of free software, //
// that may  mean that  it is  complicated to  manipulate, and  that also //
// therefore means  that it  is reserved  for developers  and experienced //
// professionals having in-depth computer  knowledge. Users are therefore //
// encouraged  to load  and test  the software's  suitability as  regards //
// their  requirements  in  conditions  enabling the  security  of  their //
// systems and/or  data to  be ensured  and, more  generally, to  use and //
// operate it in the same conditions as regards security.		  //
// 									  //
// The fact that  you are presently reading this means  that you have had //
// knowledge of the CeCILL-B license and that you accept its terms.	  //
////////////////////////////////////////////////////////////////////////////

#ifndef _Obelix_Broceliande_GroundTruth_hpp
#define _Obelix_Broceliande_GroundTruth_hpp

#include <iostream>
#include <numeric>      // std::partial_sum
#include <time.h>
#include <vector>
#include <boost/thread.hpp>

#include "Appli/OptRate.hpp"
#include "Border.hpp"
#include "obelixDebug.hpp"
#include "obelixThreads.hpp"
#include "obelixGeo.hpp"
#include "IImage.hpp"

#include "sort.hpp"
#include "vectorMisc.hpp"

namespace obelix {
  namespace broceliande {
    using namespace std;

    typedef uint8_t BroceliandeLabelType;

    // ================================================================================
    /*!
     * Classe permettant d'utiliser la vérité terrain
     */
    class GroundTruth {
    private:

      /*! Nombre de coeurs, utilisé pour la parallélisation */
      size_t coreCount;

      /*! position des tags rnd / bg=tags[0] / tags[1] ... tags[n] */
      vector<DimImg> tagsPos;
      /*! position des samples rnd / bg=tags[0] / tags[1] ... tags[n] */
      vector<DimImg> samplesPos;

      /*! Pixels étiquetés */
      vector<vector<DimImg> > tagsIdx;

      /*! Pixels étiquetés positifs tirés au hasard */
      vector<vector<DimImg> > tagsIdxIdx;

      /*! Pixels tirés au hasard et supposés négatifs */
      vector<DimImg> bgRndIdx;

      /*! Pixels tirés au hasard dans bgRndIdx, ou les 95% de pixels qui sont le plus distant avec la moyen des pixels étiquetés positifs */
      vector<DimImg> bgRndIdxIdx;

      /*! parcourt tous les index d'un ensemble principal (index direct) */
      template<typename Funct>
      void forEachSet (const vector<DimImg> &tab, const BroceliandeLabelType &tagClass, const Funct &lambda
		       /*lambda (const BroceliandeLabelType &tagClass, const DimImg &pixelId)*/) const;

      /*! parcourt tous les index d'un ensemble secondaire (index d'index) */
      template<typename Funct>
      void forEachSet (const vector<DimImg> &tabIdx,
		       const vector<DimImg> &tab, const BroceliandeLabelType &tagClass, const Funct &lambda
		       /*lambda (const BroceliandeLabelType &tagClass, const DimImg &pixelId)*/) const;

      /*! transforme le vecteur en une suite d'entiers de 0 à max-1 */
      static void createSequence (vector<DimImg> &tmpIdx, const DimImg &max);
      /*! transforme le vecteur en count valeurs différentes entre 0 est max-1 */
      static void randIdx (vector<DimImg> &tmpIdx, const DimImg &max, const DimImg &count);
      void resetTagsPos ();
      void resetSamplesPos ();

    public:
      /*! different ensemble que l'utilisateur de GroundTruth peut manipuler */
      enum SetType { tagSet, sampleSet };

      /*! parcourt tous les index d'un ensemble */
      template<typename Funct>
      void forEachIdx (const SetType &setType, const Funct &lambda
		       /*lambda (const BroceliandeLabelType &tagClass, const DimImg &pixelId)*/) const;

      inline DimImg getCount (const SetType &setType) const;

      GroundTruth (const size_t &coreCount = boost::thread::hardware_concurrency ());
      ~GroundTruth ();

      /*! Recensement tout les pixels des classes */
      template <typename FuncGetClassIdx>
      inline void setTag (const DimImg &pixelsCount, const BroceliandeLabelType &classCount, const FuncGetClassIdx &getClassIdx);

      /*! Choisit de manière aléatoire les pixels dans l'image qu'on suppose d'une autre classe que les pixels étiquetés */
      template <typename BoolFuncFree>
      inline void chooseBgRnd (const DimImg &pixelsCount, const DimImg &borderCount, const OptRate &bgTagRate, const OptRate &bgRate, const BoolFuncFree &isFree);

      void chooseSamples (const OptRate &bgRate, const OptRate &bgTagRate, const OptRate &tagsRate);

      /*! Supprime tous les index présents dans sortedBadFg dans les index directs */
      void cleanTag (const vector<DimImg> &sortedBadTag);
      /*! Supprime tous les index présents dans sortedBadFg dans les index indirects */
      void cleanSamples (const vector<DimImg> &sortedBadSamples);

      void showGroundTruth (const Border<2> &border, const string &filename, const IImage<2> &inputImage, const Point<2> &topLeft) const;
   };

#include "GroundTruth.tpp"

    // ================================================================================
  } // broceliande
} // obelix

#endif // _Obelix_Broceliande_GroundTruth_hpp
