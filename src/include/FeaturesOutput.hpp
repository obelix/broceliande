////////////////////////////////////////////////////////////////////////////
// Copyright IRISA 2017							  //
// 									  //
// triskele.obelix (at) irisa.fr					  //
// 									  //
// This  software is  a computer  program  whose purpose  is to  classify //
// pixels by using TRISKELE and random forest.				  //
// 									  //
// This software is governed by the CeCILL-B license under French law and //
// abiding by  the rules of distribution  of free software. You  can use, //
// modify  and/or  redistribute  the  software under  the  terms  of  the //
// CeCILL-B license as circulated by CEA, CNRS and INRIA at the following //
// URL "http://www.cecill.info".					  //
// 									  //
// As a counterpart to the access to  the source code and rights to copy, //
// modify and  redistribute granted  by the  license, users  are provided //
// only with a limited warranty and  the software's author, the holder of //
// the economic  rights, and the  successive licensors have  only limited //
// liability.								  //
// 									  //
// In this respect, the user's attention is drawn to the risks associated //
// with loading,  using, modifying  and/or developing or  reproducing the //
// software by the user in light of its specific status of free software, //
// that may  mean that  it is  complicated to  manipulate, and  that also //
// therefore means  that it  is reserved  for developers  and experienced //
// professionals having in-depth computer  knowledge. Users are therefore //
// encouraged  to load  and test  the software's  suitability as  regards //
// their  requirements  in  conditions  enabling the  security  of  their //
// systems and/or  data to  be ensured  and, more  generally, to  use and //
// operate it in the same conditions as regards security.		  //
// 									  //
// The fact that  you are presently reading this means  that you have had //
// knowledge of the CeCILL-B license and that you accept its terms.	  //
////////////////////////////////////////////////////////////////////////////

#ifndef _Obelix_Broceliande_FeaturesOutput_hpp
#define _Obelix_Broceliande_FeaturesOutput_hpp

#include <vector>

#include <shark/Data/Dataset.h>

#include "IImage.hpp"
#include "Appli/OptChannel.hpp"

#include "obelixGeo.hpp"

namespace obelix {
  namespace broceliande {

    using namespace shark;
    using namespace std;
    
    template <typename PixelT> class Broceliande;
    template <typename PixelT> class FeaturesOutput;

    template<typename FeatureT>
    class FeatureOutput {
    public:
      /** PixelT, DimImg or double*/
      FeatureOutputType				featureOutputType;
      /** origin bands to copy */
      vector<vector<FeatureT> >			orig4Copy; // [copyRank][leafIdx]
      /** origin bands not copy but used by DAP */
      vector<vector<FeatureT> >			orig4Dap;
      /** ndvi bands */
      vector<Raster<FeatureT, 2> >		ndviRasters;
      /** pantex bands */
      vector<Raster<FeatureT, 2> >		pantexRasters;
      /** sobel bands */
      vector<Raster<FeatureT, 2> >		sobelRasters;
      /** shortcut to copy, ndvi, pantex, sobel, ap values */
      vector<FeatureT*>				copySrcData;
      
      vector<DimChannel>			localIdx;
      vector<vector<vector<FeatureT> > >	apOutput; // [ap][thresholdIdx]leafIdx]
      vector<vector<FeatureT*> >		dapSrcIdx;
      vector<vector<DimChannel> >		dapChannelIdx;
      vector<bool>				dapDoWeight;
      
      vector<FeatureT>		&getOrig4Copy (const DimChannel &inputId) { return orig4Copy [localIdx[inputId]]; }
      vector<FeatureT>		&getOrig4Dap (const DimChannel &inputId) { return orig4Dap [localIdx[inputId]]; }
      Raster<FeatureT, 2>	&getNdvi (const DimChannel &inputId) { return ndviRasters [localIdx[inputId]]; }
      Raster<FeatureT, 2>	&getPantex (const DimChannel &inputId) { return pantexRasters [localIdx[inputId]]; }
      Raster<FeatureT, 2>	&getSobel (const DimChannel &inputId) { return sobelRasters [localIdx[inputId]]; }
      /** assume inputId is an AP band */
      vector<vector<FeatureT> >	&getAp (const DimChannel &inputId) { return apOutput [localIdx[inputId]]; }

      template <typename PixelT>
      void			init (const FeaturesOutput<PixelT> &featuresOutput, const FeatureOutputType &featureOutputType);
      void			setMatrixRowData (RealMatrix &inputs, DimChannel &channel, const size_t &row, const DimImg &pixelId) const;
      template <typename PixelT>
      void 			writeFeatures (Broceliande<PixelT> &broceliande, DimChannel &channel);

      FeatureT*			getSrcOrig4Dap (const InputChannel &inputChannel);
   };
    
    template <typename PixelT>
    class FeaturesOutput {
    public:
      const Broceliande<PixelT>	&broceliande;
      vector<FeatureOutputType>	channelsFeaturesOutputType;
      FeatureOutput<PixelT>	pixelFeature;
      FeatureOutput<DimImgPack>	dimFeature;
      FeatureOutput<DimChannel>	channelFeature;
      FeatureOutput<double>	doubleFeature;

      FeaturesOutput (const Broceliande<PixelT> &broceliande);
      ~FeaturesOutput ();

      void setMatrixRowData (RealMatrix &inputs, DimChannel &channel, const size_t &row, const DimImg &pixelId) const;
      void writeFeatures (Broceliande<PixelT> &broceliande, DimChannel &channel);
    };

    // ================================================================================
  } // broceliande
} // obelix

#endif // _Obelix_Broceliande_FeaturesOutput_hpp
