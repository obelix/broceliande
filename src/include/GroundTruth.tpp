////////////////////////////////////////////////////////////////////////////
// Copyright IRISA 2017							  //
// 									  //
// triskele.obelix (at) irisa.fr					  //
// 									  //
// This  software is  a computer  program  whose purpose  is to  classify //
// pixels by using TRISKELE and random forest.				  //
// 									  //
// This software is governed by the CeCILL-B license under French law and //
// abiding by  the rules of distribution  of free software. You  can use, //
// modify  and/or  redistribute  the  software under  the  terms  of  the //
// CeCILL-B license as circulated by CEA, CNRS and INRIA at the following //
// URL "http://www.cecill.info".					  //
// 									  //
// As a counterpart to the access to  the source code and rights to copy, //
// modify and  redistribute granted  by the  license, users  are provided //
// only with a limited warranty and  the software's author, the holder of //
// the economic  rights, and the  successive licensors have  only limited //
// liability.								  //
// 									  //
// In this respect, the user's attention is drawn to the risks associated //
// with loading,  using, modifying  and/or developing or  reproducing the //
// software by the user in light of its specific status of free software, //
// that may  mean that  it is  complicated to  manipulate, and  that also //
// therefore means  that it  is reserved  for developers  and experienced //
// professionals having in-depth computer  knowledge. Users are therefore //
// encouraged  to load  and test  the software's  suitability as  regards //
// their  requirements  in  conditions  enabling the  security  of  their //
// systems and/or  data to  be ensured  and, more  generally, to  use and //
// operate it in the same conditions as regards security.		  //
// 									  //
// The fact that  you are presently reading this means  that you have had //
// knowledge of the CeCILL-B license and that you accept its terms.	  //
////////////////////////////////////////////////////////////////////////////

#ifndef _Obelix_Broceliande_GroundTruth_tpp
#define _Obelix_Broceliande_GroundTruth_tpp

// ================================================================================
inline DimImg
GroundTruth::getCount (const SetType &setType) const {
  switch (setType) {
  case   tagSet: return tagsPos[tagsPos.size ()-1];
  case   sampleSet: return samplesPos[samplesPos.size ()-1];
  }
  BOOST_ASSERT (false);
  return 0;
}

// ----------------------------------------
template<typename Funct>
inline void
GroundTruth::forEachSet (const vector<DimImg> &tab, const BroceliandeLabelType &tagClass, const Funct &lambda
			 /*lambda (const BroceliandeLabelType &tagClass, const DimImg &idxReal)*/) const {
  DEF_LOG ("GroundTruth::forEachSet", "tagClass:" << tagClass << " size:" << tab.size ());
  for (DimImg idxReal : tab)
    lambda (tagClass, idxReal);
}

// ----------------------------------------
template<typename Funct>
inline void
GroundTruth::forEachSet (const vector<DimImg> &tabIdx,
			 const vector<DimImg> &tab, const BroceliandeLabelType &tagClass, const Funct &lambda
			 /*lambda (const BroceliandeLabelType &tagClass, const DimImg &idxReal)*/) const {
  DEF_LOG ("GroundTruth::forEachSet", "tagClass:" << tagClass << " size:" << tabIdx.size () << " form size:" << tab.size ());
  for (DimImg idxIdx : tabIdx)
    lambda (tagClass, tab [idxIdx]);
}

// ----------------------------------------
template<typename Funct>
inline void
GroundTruth::forEachIdx (const SetType &setType, const Funct &lambda
			 /*lambda (const BroceliandeLabelType &tagClass, const DimImg &pixelId)*/) const {
  switch (setType) {
  case tagSet:
    forEachSet (bgRndIdx, 0, lambda);
    for (BroceliandeLabelType classIdx = 0; classIdx < tagsIdx.size (); ++classIdx)
      forEachSet (tagsIdx[classIdx], classIdx, lambda);
    break;
  case sampleSet:
    forEachSet (bgRndIdxIdx, bgRndIdx,  0, lambda);
    for (BroceliandeLabelType classIdx = 0; classIdx < tagsIdxIdx.size (); ++classIdx)
      forEachSet (tagsIdxIdx[classIdx], tagsIdx[classIdx], classIdx, lambda);
    break;
  default:
    BOOST_ASSERT (false);
  }
}


// ================================================================================
template <typename FuncGetClassIdx>
inline void
GroundTruth::setTag (const DimImg &pixelsCount, const BroceliandeLabelType &classCount, const FuncGetClassIdx &getClassIdx) {
  DEF_LOG ("GroundTruth::setTag", "pixelsCount: " << pixelsCount << " classCount:" << classCount);
  vector <vector<DimImg> > classIdxCount (coreCount+1, vector<DimImg> (classCount+1, 0));
  // Compte le nombre de pixels par classes dans chaque tuile
  dealThread (pixelsCount, coreCount,
	      [this, &classIdxCount, &getClassIdx](const size_t &threadId, const DimImg &minVal, const DimImg &maxVal) {
		DimImg *classIdxCountTab (classIdxCount [threadId+1].data ());
		BroceliandeLabelType classIdx (0);
		for (DimImg id = minVal; id < maxVal; ++id)
		  if (getClassIdx (id, classIdx))
		    ++classIdxCountTab[classIdx];
	      });
  // commule les nombres et réserve les classes
  tagsIdx = vector<vector<DimImg> > (classCount+1);
  for (BroceliandeLabelType classIdx = 0; classIdx < classCount+1; ++classIdx) {
    for (size_t threadId = 0; threadId < coreCount; ++threadId) {
      classIdxCount [threadId+1][classIdx] += classIdxCount [threadId][classIdx];
      LOG ("classIdxCount[" << threadId << "][" << classIdx << "]: " << classIdxCount [threadId+1][classIdx]);
    }
    tagsIdx[classIdx].resize (classIdxCount[coreCount][classIdx]);
    LOG ("tagsIdx[" << classIdx << "]: " << tagsIdx[classIdx].size ());
  }
  bgRndIdx.resize (0);
  tagsIdxIdx = vector<vector<DimImg> > ();
  bgRndIdxIdx.resize (0);
  resetTagsPos ();
  samplesPos.clear ();
  // recensement effectif
  dealThread (pixelsCount, coreCount,
	      [this, &classIdxCount, &getClassIdx](const size_t &threadId, const DimImg &minVal, const DimImg &maxVal) {
		DimImg *classIdxCountTab (classIdxCount [threadId].data ());
		BroceliandeLabelType classIdx (0);
		for (DimImg id = minVal; id < maxVal; ++id)
		  if (getClassIdx (id, classIdx))
		    tagsIdx[classIdx][classIdxCountTab[classIdx]++] = id;
	      });
}

// ================================================================================
template <typename BoolFuncFree>
inline void
GroundTruth::chooseBgRnd (const DimImg &pixelsCount, const DimImg &borderCount, const OptRate &bgTagRate, const OptRate &bgRate, const BoolFuncFree &isFree) {
  DEF_LOG ("GroundTruth::chooseBgRnd", "pixelsCount: " << pixelsCount << " bgTagRate: " << bgTagRate << " bgRate: " << bgRate);
  const DimImg bgTagIdxCount (tagsPos[1]);
  //const DimImg classIdxCount (getCount (tagSet)-bgTagIdxCount);
  const DimImg unknownCount = pixelsCount - borderCount - getCount (tagSet);

  DimImg maxTagCount (0);
  for (BroceliandeLabelType classIdx = 1; classIdx < tagsIdx.size (); ++classIdx)
    maxTagCount = max (maxTagCount, DimImg (tagsIdx[classIdx].size ()));
  const DimImg bgCount = bgRate.getRate (maxTagCount);
  const DimImg bgAvailableTag = bgTagRate.getRate (bgTagIdxCount);
  const DimImg bgRndCount = min (unknownCount, bgCount-bgAvailableTag);

  if (bgRndCount < unknownCount >> 10) {
   LOG ("random loop: " << bgRndCount);
   bgRndIdx.resize (bgRndCount);
    // Le nombre d'essais possible correspond au quart du nombre de pixels pris dans le background
    DimImg tryCount = bgRndCount/4;
    for (DimImg idx = 0; idx < bgRndCount; ) {
      // On tire un pixel au hasard dans l'image
      DimImg tested = rand () % pixelsCount;
      // Si ce pixel est dans les pixels étiquetés ou dans discover, on incrémente le nombre d'essais réalisés
      if (!isFree (tested)) {
	if (tryCount) {
	  tryCount--;
	  // On recommence le tirage
	  continue;
	}
	LOG ("too much try");
	do
	  tested = (tested+1) % pixelsCount;
	while (!isFree (tested));
      }
      bgRndIdx [idx++] = tested;
    }
  } else {
    LOG ("set all unknown: " << unknownCount);
    bgRndIdx.resize (unknownCount);
    DimImg size = 0;
    for (DimImg idx = 0; idx < pixelsCount; ++idx)
      if (isFree (idx))
	bgRndIdx [size++] = idx;
    random_shuffle (bgRndIdx.begin (), bgRndIdx.end ());
    bgRndIdx.resize (bgRndCount);
    // XXX libérer mémoire ?
  }
  resetTagsPos ();
  samplesPos.clear ();
  LOG ("bgRndIdx size: " << bgRndIdx.size () << " bgTdx: " << tagsIdx[0].size ());
}

// ================================================================================
#endif // _Obelix_Broceliande_GroundTruth_tpp

