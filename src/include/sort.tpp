////////////////////////////////////////////////////////////////////////////
// Copyright IRISA 2017							  //
// 									  //
// triskele.obelix (at) irisa.fr					  //
// 									  //
// This  software is  a computer  program  whose purpose  is to  classify //
// pixels by using TRISKELE and random forest.				  //
// 									  //
// This software is governed by the CeCILL-B license under French law and //
// abiding by  the rules of distribution  of free software. You  can use, //
// modify  and/or  redistribute  the  software under  the  terms  of  the //
// CeCILL-B license as circulated by CEA, CNRS and INRIA at the following //
// URL "http://www.cecill.info".					  //
// 									  //
// As a counterpart to the access to  the source code and rights to copy, //
// modify and  redistribute granted  by the  license, users  are provided //
// only with a limited warranty and  the software's author, the holder of //
// the economic  rights, and the  successive licensors have  only limited //
// liability.								  //
// 									  //
// In this respect, the user's attention is drawn to the risks associated //
// with loading,  using, modifying  and/or developing or  reproducing the //
// software by the user in light of its specific status of free software, //
// that may  mean that  it is  complicated to  manipulate, and  that also //
// therefore means  that it  is reserved  for developers  and experienced //
// professionals having in-depth computer  knowledge. Users are therefore //
// encouraged  to load  and test  the software's  suitability as  regards //
// their  requirements  in  conditions  enabling the  security  of  their //
// systems and/or  data to  be ensured  and, more  generally, to  use and //
// operate it in the same conditions as regards security.		  //
// 									  //
// The fact that  you are presently reading this means  that you have had //
// knowledge of the CeCILL-B license and that you accept its terms.	  //
////////////////////////////////////////////////////////////////////////////


// ================================================================================
template <typename T>
inline vector<T> getRandReduce (vector<T> index, T reduceCount) {
  T size = index.size ();
  if (reduceCount >= size)
    return index;
  for (T i = 0; i < size; ++i) {
    T j = rand () % size;
    T tmp = index [i];
    index [i] = index [j];
    index [j] = tmp;
  }
  vector<T> result (index.begin (), index.begin ()+reduceCount);
  return result;
}

template <typename E, typename WeightT, typename DimImg, typename WeightFunct>
inline DimImg mergeSort (E *dst, const vector<E *> &tab, const vector<DimImg> &tabSize, const WeightFunct &weightFunct) {
  DimImg count = 0;
  vector<DimImg> curIdx (tab.size (), 0);
  for (;;) {
    int minIdx = -1;
    WeightT minLevel = 0;
    for (DimImg i = 0; i < curIdx.size (); ++i) {
      if (curIdx[i] >= tabSize[i])
	continue;
      if (minIdx >= 0 &&
	  !weightFunct.isWeightInf (tab[i][curIdx[i]], minLevel))
	continue;
      minIdx = i;
      minLevel = tab[i][curIdx[i]];
      // LOG ("set minIdx:" << minIdx << " minLevel:" << minLevel);
    }
    if (minIdx < 0)
      break;
    *dst++ = tab[minIdx][curIdx[minIdx]++];
    ++count;
  }
  return count;
}

// ================================================================================
