////////////////////////////////////////////////////////////////////////////
// Copyright IRISA 2017							  //
// 									  //
// triskele.obelix (at) irisa.fr					  //
// 									  //
// This  software is  a computer  program  whose purpose  is to  classify //
// pixels by using TRISKELE and random forest.				  //
// 									  //
// This software is governed by the CeCILL-B license under French law and //
// abiding by  the rules of distribution  of free software. You  can use, //
// modify  and/or  redistribute  the  software under  the  terms  of  the //
// CeCILL-B license as circulated by CEA, CNRS and INRIA at the following //
// URL "http://www.cecill.info".					  //
// 									  //
// As a counterpart to the access to  the source code and rights to copy, //
// modify and  redistribute granted  by the  license, users  are provided //
// only with a limited warranty and  the software's author, the holder of //
// the economic  rights, and the  successive licensors have  only limited //
// liability.								  //
// 									  //
// In this respect, the user's attention is drawn to the risks associated //
// with loading,  using, modifying  and/or developing or  reproducing the //
// software by the user in light of its specific status of free software, //
// that may  mean that  it is  complicated to  manipulate, and  that also //
// therefore means  that it  is reserved  for developers  and experienced //
// professionals having in-depth computer  knowledge. Users are therefore //
// encouraged  to load  and test  the software's  suitability as  regards //
// their  requirements  in  conditions  enabling the  security  of  their //
// systems and/or  data to  be ensured  and, more  generally, to  use and //
// operate it in the same conditions as regards security.		  //
// 									  //
// The fact that  you are presently reading this means  that you have had //
// knowledge of the CeCILL-B license and that you accept its terms.	  //
////////////////////////////////////////////////////////////////////////////

#ifndef _Obelix_Broceliande_Broceliande_hpp
#define _Obelix_Broceliande_Broceliande_hpp

#include <cstdint>

#include <shark/Data/Dataset.h>
#include <shark/Models/Trees/RFClassifier.h>
#include <shark/Data/WeightedDataset.h>

#include "ArrayTree/ArrayTreeBuilder.hpp"
#include "ArrayTree/GraphWalker.hpp"
#include "Border.hpp"
#include "IImage.hpp"
#include "IntegralImage.hpp"
#include "obelixGeo.hpp"
#include "Attributes/AttributesCache.hpp"

#include "OptBroceliande.hpp"
#include "TextWin.hpp"
#include "GroundTruth.hpp"
#include "FeaturesOutput.hpp"

#define _BroceliandeRealVectorSharkLabelType

namespace obelix {
  namespace broceliande {

    using namespace triskele;
    using namespace shark;

#ifdef _BroceliandeRealVectorSharkLabelType
    typedef RealVector SharkLabelType;
#else
    typedef unsigned int SharkLabelType;
#endif
    

    // ================================================================================
    template <typename PixelT>
    class Broceliande {
    public:
      Broceliande (IImage<2> &inputImage, IImage<2> &outputImage,
		   const Point<2> &topLeft, const Size<2> &size, OptBroceliande &optBroceliande);
      ~Broceliande ();

      inline const Size<2> &getSize () const { return size; }
      inline const DimImg &getPixelCount () const { return pixelCount; }
      inline const FeaturesOutput<PixelT> &getFeaturesOutput () const { return featuresOutput; }

      /*! \brief read and build all bands, build trees, produce all profiles */
      void parseInput ();
      /*! \brief write all profiles instead prediction */
      template <typename FeatureT, typename APFunct>
      void doFeature (FeatureOutput<FeatureT> &featureOutput, ArrayTreeBuilder<PixelT, PixelT, 2> &atb, AttributesCache<PixelT, 2> &attributesCache, const APFunct &setAP, const map <AttributeType, const InputChannel*> &attrProd);
      void snapshot ();
      /*! \brief entraine une machine d'apprentissage */
      void training (RFClassifier<SharkLabelType> &classifier); // avec template PixelGT / showClassif
      /*! \brief predi une classification */
      void predict (RFClassifier<SharkLabelType> &classifier);

      template <typename PixelGT>
      bool chooseGroundTruth (IImage<2> &groundTruthImage, const PixelGT &bgValue);

      void showGroundTruth (const string &filename, const IImage<2> &groundTruthImage) const;
      int chooseSamples ();
      void setMatrixRowData (RealMatrix &inputs, const size_t &row, const DimImg &pixelId) const;
      void getSamples (LabeledData<RealVector, SharkLabelType> &data, const GroundTruth::SetType &setType) const;
      DimImg getData (Data<RealVector> &data, DimImg pixelId, const DimImg &maxDataLeft) const;

      template <typename FeatureT>
      void writeBand (FeatureT *pixels, const DimChannel &channel);

      OptChannel &optChannel;

    private:
      IImage<2> &inputImage;
      IImage<2> &outputImage;
      bool noWriteFlag;
      OptBroceliande &optBroceliande;

      Point<2> topLeft;
      Size<2> size;
      Border<2> border;
      GraphWalker<2> graphWalker;
      DimImg pixelCount;
      GroundTruth groundTruth;
      DimImg allBandsCount;

      Raster<PixelT, 2> inputRaster;
      Raster<PixelT, 2> outputPixRaster;
      Raster<PixelT, 2> &getProducerRaster (const InputChannel &inputChannel); // XXX ?

      vector<vector<double> > integralImages, doubleIntegralImages; // [iiIdx][leafIdx]
      vector<vector<TextWin<2> > > haars, stats; // [textIdx][sizeIdx]

      FeaturesOutput<PixelT> featuresOutput;
      void saveOrig4Copy (const Raster<PixelT, 2> &raster, const DimChannel &inputId);
      void saveOrig4Dap (const Raster<PixelT, 2> &raster, const DimChannel &inputId);

      vector<DimChannel> iiHaarIdx, iiStatIdx;
      vector<DimChannel> diiStatIdx;

      Tree<2> tree;
      WeightAttributes<PixelT, 2> weightAttributes;

      DimImg getBatchSize (const DimImg &maxDataLeft) const;

      void cleanTags ();
      void cleanSamples ();
    };

    // ================================================================================
  } // broceliande
} // obelix

#endif // _Obelix_Broceliande_Broceliande_hpp
