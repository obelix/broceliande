////////////////////////////////////////////////////////////////////////////
// Copyright IRISA 2017							  //
// 									  //
// triskele.obelix (at) irisa.fr					  //
// 									  //
// This  software is  a computer  program  whose purpose  is to  classify //
// pixels by using TRISKELE and random forest.				  //
// 									  //
// This software is governed by the CeCILL-B license under French law and //
// abiding by  the rules of distribution  of free software. You  can use, //
// modify  and/or  redistribute  the  software under  the  terms  of  the //
// CeCILL-B license as circulated by CEA, CNRS and INRIA at the following //
// URL "http://www.cecill.info".					  //
// 									  //
// As a counterpart to the access to  the source code and rights to copy, //
// modify and  redistribute granted  by the  license, users  are provided //
// only with a limited warranty and  the software's author, the holder of //
// the economic  rights, and the  successive licensors have  only limited //
// liability.								  //
// 									  //
// In this respect, the user's attention is drawn to the risks associated //
// with loading,  using, modifying  and/or developing or  reproducing the //
// software by the user in light of its specific status of free software, //
// that may  mean that  it is  complicated to  manipulate, and  that also //
// therefore means  that it  is reserved  for developers  and experienced //
// professionals having in-depth computer  knowledge. Users are therefore //
// encouraged  to load  and test  the software's  suitability as  regards //
// their  requirements  in  conditions  enabling the  security  of  their //
// systems and/or  data to  be ensured  and, more  generally, to  use and //
// operate it in the same conditions as regards security.		  //
// 									  //
// The fact that  you are presently reading this means  that you have had //
// knowledge of the CeCILL-B license and that you accept its terms.	  //
////////////////////////////////////////////////////////////////////////////

#include <shark/Data/DataDistribution.h>
#include <shark/Algorithms/Trainers/RFTrainer.h>
#include <shark/ObjectiveFunctions/Loss/ZeroOneLoss.h>

using namespace shark;

#define CATCH_CONFIG_MAIN    /* Let Catch provide main() */
#include <catch.hpp> /* ajouter l'option -I /usr/include/catch2/ dans MakefileNoOTB si besoin */

using namespace std;

void
testSave () {
  PamiToy generator (5, 5, 0, 0.4);
  auto train = generator.generateDataset (200);
  auto test = generator.generateDataset (200);

  RFTrainer<unsigned int> trainer (true, true);
  RFClassifier<unsigned int> model2;
  trainer.train (model2, train);

  string filename = "data/saveRFsample.txt";

  ofstream ofs (filename.c_str ());
  //OutArchive oa (ofs);
  TextOutArchive oa (ofs);
  model2.save (oa, 0);
  ofs.flush ();
  ofs.close ();

  RFClassifier<unsigned int> model;
  ifstream ifs (filename.c_str ());
  //InArchive ia (ifs);
  TextInArchive ia (ifs);
  model.load (ia, 0);

  ZeroOneLoss<unsigned int, RealMatrix> loss;
  double error_train = loss.eval (train.labels (), model (train.inputs ()));

  ZeroOneLoss<unsigned int, RealMatrix> loss2;
  double error_train2 = loss2.eval (train.labels (), model2 (train.inputs ()));

  // double error_test = loss.eval (test.labels (), model (test.inputs ()));

  REQUIRE (true); // XXX test
  cout << "error : " << error_train << endl;
}

TEST_CASE ("Testing Save RF...") {
  testSave ();
}
