////////////////////////////////////////////////////////////////////////////
// Copyright IRISA 2017							  //
// 									  //
// triskele.obelix (at) irisa.fr					  //
// 									  //
// This  software is  a computer  program  whose purpose  is to  classify //
// pixels by using TRISKELE and random forest.				  //
// 									  //
// This software is governed by the CeCILL-B license under French law and //
// abiding by  the rules of distribution  of free software. You  can use, //
// modify  and/or  redistribute  the  software under  the  terms  of  the //
// CeCILL-B license as circulated by CEA, CNRS and INRIA at the following //
// URL "http://www.cecill.info".					  //
// 									  //
// As a counterpart to the access to  the source code and rights to copy, //
// modify and  redistribute granted  by the  license, users  are provided //
// only with a limited warranty and  the software's author, the holder of //
// the economic  rights, and the  successive licensors have  only limited //
// liability.								  //
// 									  //
// In this respect, the user's attention is drawn to the risks associated //
// with loading,  using, modifying  and/or developing or  reproducing the //
// software by the user in light of its specific status of free software, //
// that may  mean that  it is  complicated to  manipulate, and  that also //
// therefore means  that it  is reserved  for developers  and experienced //
// professionals having in-depth computer  knowledge. Users are therefore //
// encouraged  to load  and test  the software's  suitability as  regards //
// their  requirements  in  conditions  enabling the  security  of  their //
// systems and/or  data to  be ensured  and, more  generally, to  use and //
// operate it in the same conditions as regards security.		  //
// 									  //
// The fact that  you are presently reading this means  that you have had //
// knowledge of the CeCILL-B license and that you accept its terms.	  //
////////////////////////////////////////////////////////////////////////////

#include <type_traits>
#include <iostream>

#include <shark/Data/Csv.h>
#include <shark/Data/Dataset.h>
#include <shark/Data/DataDistribution.h>
#include <shark/Algorithms/Trainers/RFTrainer.h>
#include <shark/ObjectiveFunctions/Loss/ZeroOneLoss.h>

using namespace std;
using namespace shark;

#define CATCH_CONFIG_MAIN    /* Let Catch provide main() */
#include <catch.hpp> /* ajouter l'option -I /usr/include/catch2/ dans MakefileNoOTB si besoin */

template <typename LabelType> void initValues (const Data<unsigned int> &labels, Data<LabelType> &labelsM);

template <>
void
initValues (const Data<unsigned int> &labels, Data<unsigned int> &labelsM) {
  size_t nbItem (labels.numberOfElements ());
  labelsM.batch (0).resize (nbItem);
  for (size_t id = 0; id < nbItem; ++id)
    labelsM.batch (0) [id] = labels.batch (0) [id];
}

template <>
void
initValues (const Data<unsigned int> &labels, Data<RealVector> &labelsM) {
  size_t nbItem (labels.numberOfElements ());
  labelsM.batch (0).resize (nbItem, 3);
  for (size_t id = 0; id < nbItem; ++id)
    for (size_t i = 0; i < 3; ++i)
      labelsM.batch (0) (id, i) = (labels.batch (0) [id] == i ? 1 : 0);
}


template <typename LabelType>
void
testShark () {

  string inputsFile ("data/testShark-inputs.csv");
  string labelsFile ("data/testShark-labels.csv");

  Data<RealVector> inputs;
  Data<unsigned int> labels;
  try {
    importCSV (inputs, inputsFile, ' ');
    importCSV (labels, labelsFile, ' ');
  } catch (...) {
    cerr << "Unable to open file " <<  inputsFile << " and/or " << labelsFile << ". Check paths!" << endl;
    exit (EXIT_FAILURE);
  }

  Data<LabelType> labelsM (1);
  initValues<LabelType> (labels, labelsM);
  LabeledData<RealVector, LabelType> data (inputs, labelsM);
  WeightedLabeledData<RealVector, LabelType> wData (data, 1);
  //WeightedLabeledData<RealVector, LabelType> wData (inputs, labelsM);

  RFTrainer<LabelType> trainer (true, true);
  RFClassifier<LabelType> model;
  trainer.train (model, wData);
  Data<LabelType> prediction = model (data.inputs ());

  auto range (prediction.elements ());
  if (is_same<LabelType, RealVector>::value) {
    for (size_t predIdx = 0; predIdx < 24; ++predIdx) {
      RealVector item (prediction.element (predIdx));
      int r (0);
      for (size_t i = 0; i < item.size (); ++i)
	if (item [i] > .5) {
	  r = i;
	  break;
	}
      cout << r << " ";
    }
    cout << endl;
  }

  for (size_t predIdx = 0; predIdx < 24; ++predIdx)
    if (is_same<LabelType, RealVector>::value) {
      RealVector item (prediction.element (predIdx));
      for (size_t i = 0; i < item.size (); ++i)
	cout << item [i] << " ";
      cout << "/ ";
    } else
      cout << prediction.element (predIdx) << " ";
      
  cout << endl;

  REQUIRE (true);
}


TEST_CASE ("Testing output RF...") {
  testShark<unsigned int> ();
  testShark<RealVector> ();
}
