# Broceliande

Broceliande aims to use TRISKELE and Random Forest for classification.

It is the use of representation trees of satellite images and decision trees to detect woody features in Europe.

Please read the [Broceliande manual](https://obelix.gitlabpages.inria.fr/triskele-doc/broceliande_manual.pdf)

and the [doxygene](https://obelix.gitlabpages.inria.fr/broceliande/doc/index.html) ressources.

## Use docker
```bash

docker run -v /myLocalDataBaseImages:/images -i -t registry.gitlab.inria.fr/obelix/broceliande/master:2.4.20200516 /bin/bash
# ./broceliande --help
```

## Compilation

```bash

sudo apt-get install --fix-missing build-essential git cmake g++ libboost-system-dev libboost-chrono-dev libboost-program-options-dev libboost-test-dev libboost-filesystem-dev libboost-date-time-dev libboost-serialization-dev libboost-thread-dev libhdf5-dev gdal-bin libgdal-dev libtbb-dev libtinyxml-dev catch bash

git clone https://gitlab.inria.fr/obelix/broceliande.git

# or for contributors :
# git clone git+ssh://git@gitlab.inria.fr/obelix/broceliande.git

cd broceliande ; git submodule init && git submodule update

cd thirdparty/shark/ ; git checkout 4.0 ; patch CMakeLists.txt ../../broceliandeShark4.patch ; cd ../..

cd thirdparty/triskele ; git checkout master && git fetch -a && git pull && git submodule init && git submodule update ; cd ../..

mkdir ../build ; cd ../build

cmake -DCMAKE_BUILD_TYPE=Release ../broceliande

# or debug mode
# cmake -DCMAKE_BUILD_TYPE=Debug ../broceliande

make -j `nproc`
```
