## DIR #################################
SRC_DIR   = src
TST_DIR   = $(SRC_DIR)/test
CPP_DIR   = $(SRC_DIR)/cpp
HPP_DIR   = $(SRC_DIR)/include
BLD_DIR   = build
OUT_DIR   = $(BLD_DIR)/out
LIB_DIR   = $(BLD_DIR)/lib
OBJ_DIR   = $(BLD_DIR)/obj

## TRISKELE #################################
TRSK_DIR = $(HOME)/Travail/TriskeleBin
TRSK_INC_DIR = $(TRSK_DIR)/include
TRSK_LIB_DIR = $(TRSK_DIR)/lib

## SHARK #################################
SHRK_DIR = $(HOME)/shark/usr/local
SHRK_INC_DIR = $(SHRK_DIR)/include
SHRK_LIB_DIR = $(SHRK_DIR)/lib

## PRG #################################

BRCLD_PRG = broceliande
BRCLD_MOD = OptGroundTruth OptGroundTruthParser OptTraining OptTrainingParser OptBatch OptBatchParser FeatureOutput OptBroceliande vectorMisc GroundTruth Broceliande
BRCLD_SRC = $(patsubst %, $(CPP_DIR)/%.cpp, $(BRCLD_PRG))
BRCLD_OBJ = $(patsubst %, $(OBJ_DIR)/%.o, $(BRCLD_MOD))
BRCLD_OUT = $(patsubst %, $(OUT_DIR)/%, $(BRCLD_PRG))

BRCLD_CFG_PRG = broceliandeConfig
BRCLD_CFG_SRC = $(patsubst %, $(CPP_DIR)/%.cpp, $(BRCLD_CFG_PRG))
BRCLD_CFG_OUT = $(patsubst %, $(OUT_DIR)/%, $(BRCLD_CFG_PRG))

TST_SAVE_PRG = saveRFsample
TST_SAVE_SRC = $(patsubst %, $(TST_DIR)/%.cpp, $(TST_SAVE_PRG))
TST_SAVE_OUT = $(patsubst %, $(OUT_DIR)/%, $(TST_SAVE_PRG))

## FLAGS ###############################
######### -DNDEBUG => warning with shark
# *** fast and no control
DFLAGS = -O2 -DNDEBUG -DNO_OTB -DBOOST_DISABLE_ASSERTS -DDISABLE_LOG
# DFLAGS = -O2 -DNDEBUG -DNO_OTB -DBOOST_DISABLE_ASSERTS
# DFLAGS = -O2 -DNDEBUG -DNO_OTB -DINTEL_TBB_THREAD -DBOOST_DISABLE_ASSERTS
# *** fast but control
# DFLAGS = -O2 -DNDEBUG -DNO_OTB -DINTEL_TBB_THREAD
# *** debug multi-threaded
# DFLAGS = -g -DENABLE_LOG -DNO_OTB -DNDEBUG
# *** debug one thread
# DFLAGS = -g -DENABLE_LOG -DNO_OTB -DTHREAD_DISABLE -DENABLE_SMART_LOG -DNDEBUG

IFLAGS = $(DFLAGS) -std=c++11 -fopenmp -MMD -I$(HPP_DIR) -I$(TRSK_INC_DIR) -I$(SHRK_INC_DIR) -I/usr/include/gdal
LFLAGS = -L$(LIB_DIR) -L$(TRSK_LIB_DIR) -L$(SHRK_LIB_DIR) -ltriskele -lstdc++ -lgomp -l pthread -lboost_system -lboost_chrono -lboost_thread -lboost_program_options -lboost_date_time -lboost_serialization -lboost_filesystem -lboost_unit_test_framework -lgdal -ltbb -ltinyxml -lshark
CC = g++

# other options : -fPIC -pthread -march=native -l m  

## RULES ###############################
$(OBJ_DIR)/%.o: $(SRC_DIR)/*/%.cpp
	$(CC) $< $(IFLAGS) -cpp -c -o $@
$(OBJ_DIR)/%.o: $(SRC_DIR)/*/*/%.cpp
	$(CC) $< $(IFLAGS) -cpp -c -o $@

$(OUT_DIR)/%: $(SRC_DIR)/*/%.cpp
	$(CC) $(IFLAGS) $< -L$(LIB_DIR) $(LFLAGS) -cpp -o $@

## ENTRIES #############################
all:	init broceliande broceliandeConfig

init:
	mkdir -p $(OUT_DIR) $(OBJ_DIR) $(LIB_DIR)

clean:
	-rm -f "data/saveRFsample.txt"
	find . -type f '(' -name '#*' -o -name '*~' ')' -print -exec rm -f '{}' \;

wipe: clean
	-rm -rf $(OBJ_DIR)
	-rm -f $(BRCLD_OUT)
	-rm -f $(OUT_DIR)/*.d
	-rmdir $(OUT_DIR) $(OBJ_DIR) $(LIB_DIR) $(BLD_DIR)

$(BRCLD_OUT): $(BRCLD_SRC) ${BRCLD_OBJ}
	$(CC) $(IFLAGS) $(BRCLD_SRC) ${BRCLD_OBJ} -L$(LIB_DIR) $(LFLAGS) -cpp -o $@

$(BRCLD_CFG_OUT): $(BRCLD_CFG_SRC) ${BRCLD_CFG_OBJ}
	$(CC) $(IFLAGS) $(BRCLD_CFG_SRC) ${BRCLD_CFG_OBJ} -L$(LIB_DIR) $(LFLAGS) -cpp -o $@

$(TST_SAVE_OUT): $(TST_SAVE_SRC)

build/obj/Broceliande.o: src/cpp/Broceliande.tcpp
src/cpp/Broceliande.tcpp: src/cpp/Broceliande.mcpp src/cpp/updateTemplate.sh
	src/cpp/updateTemplate.sh Broceliande.mcpp Broceliande.tcpp

build/obj/FeaturesOutput.o: src/cpp/FeaturesOutput.tcpp
src/cpp/FeaturesOutput.tcpp: src/cpp/FeaturesOutput.mcpp src/cpp/updateTemplate.sh
	src/cpp/updateTemplate.sh FeaturesOutput.mcpp FeaturesOutput.tcpp

testSave: init $(TST_SAVE_OUT)
	$(TST_SAVE_OUT)

broceliande: init $(BRCLD_OUT)
	$(BRCLD_OUT) --loadChannel /home/felix/Travail/svn/broceliande/data/config.xml --includeNoDataFlag \
		/home/DMZ/data/satellite/panel/BroceliandeTest.tif \
		/home/felix/Travail/svn/broceliande/result/output.tif \
		--coreCount 1
		# --groundTruth /home/DMZ/data/satellite/panel/BroceliandeTestGT.tif \
		# --batchDir batch/
	# $(BRCLD_OUT) --version
	# $(BRCLD_OUT) --help


broceliandeConfig: init $(BRCLD_CFG_OUT)
	$(BRCLD_CFG_OUT) --loadChannel /home/felix/Travail/svn/broceliande/data/config.xml --includeNoDataFlag \
		/home/DMZ/data/satellite/panel/BroceliandeTest.tif \
		/home/felix/Travail/svn/broceliande/result/output.tif \
		--coreCount 1

## DEPENDS #############################
ALL_OUT = $(BRCLD_OUT) $(BRCLDC_OUT) $(TST_SAVE_OUT)
ALL_OBJ = $(BRCLD_OBJ) $(BRCLDC_OBJ) $(TST_SAVE_OBJ)

DEPENDS = ${ALL_OUT:=.d} ${ALL_OBJ:.o=.d}
-include ${DEPENDS}

########################################
